<?php
/**
 * Velocity User Class - velocity-user-class.php
 * PHP Version 5 and +
 * @package v.user.class.php
 * @link https://velocity-framework.com/php/user.php
 * @author Juan Pablo Casabianca <jpc@pixelstudio32.com>
 * @copyright 2014 Juan Pablo Casabianca, GRUPO CASABIANCA IREGUI SAS
 * @copyright 2014 PHPACADEMY - As learned in there tutorial
 * @license http://www.http://creativecommons.org/licenses/by-nc-nd/4.0/
 * Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)
 */

namespace Velocity\Users;

use Velocity\Db\Db;
use Velocity\Config\Config;
use Velocity\Authentication\Session;
use Velocity\Authentication\Validate;
use Velocity\Authentication\Cookie;
use Velocity\Security\Hash;

class User_admin {
	private $_db,
			$_data,
			$_sessionName,
			$_cookieName,
			$_isLoggedIn;

	public function __construct($user = null) {
		$this->_db = Db::getInstance();
		$this->_sessionName = Config::get('session/session_name');
		$this->_cookieName = Config::get('remember/cookie_name');
		if(!$user) {
			if(Session::exists($this->_sessionName)) {
				$user = Session::get($this->_sessionName);
				if($this->find($user)) {
					$this->_isLoggedIn = true;
				} else {
					// Process Logout
				}
			}
		} else {
			$this->find($user);
		}

	}

	public function update($fields = array(), $id = null) {

		if(!$id && $this->isLoggedIn()) {
			$id = $this->data()->id;
		}

		if(!$this->_db->update('v_users_admin', $id, $fields)) {
			throw new Exception('There was a problem updating!');
		}
	}

	public function create($fields = array()) {
		if(!$this->_db->insert('v_users_admin', $fields)) {
			throw new Exception('There was a problem creating an account.');
		}
	}

	public function find($user = null) {
		if(Validate::check_if_email($user)) {
			$field = 'email';
		} else {
			$field = (is_numeric($user)) ? 'id' : 'username';
		}
		$data = $this->_db->get('v_users_admin', array($field, '=', $user));

		if($data->count()) {
			$this->_data = $data->first();
			return true;
		}
		return false;
	}

	public function login($username = null,$password = null,$remember = false) {
		
		if(!$username && !$password && $this->exists()) {
			Session::put($this->_sessionName, $this->data()->id);
		} else {
			$user = $this->find($username);
			if($user) {
				if($this->data()->password === Hash::make($password, $this->data()->salt)) {
					Session::put($this->_sessionName, $this->data()->id);

					if($remember) {
						$hash = Hash::unique();
						$hashCheck = $this->_db->get('v_users_admin_session', array('user_id','=', $this->data()->id));

						if(!$hashCheck->count()) {
							$this->_db->insert('v_users_admin_session', array(
								'user_id' => $this->data()->id,
								'hash' => $hash
							));
						} else {
							$hash = $hashCheck->first()->hash;
						}

						Cookie::put($this->_cookieName, $hash, Config::get('remember/cookie_expiry'));

					}

					return true;
				}
			} 
		}
		return false;
	}

	public function login_after_activation($username = null) {
		if(!$username && $this->exists()) {
			Session::put($this->_sessionName, $this->data()->id);
		} else {
			$user = $this->find($username);
			if($user) {
				Session::put($this->_sessionName, $this->data()->id);
				return true;
			}
		}
		return false;
	}

	public function hasPermission($key) {
		$group = $this->_db->get('v_groups', array('id', '=', $this->data()->group . "ORDER BY id DESC"));
		if($group->count()) {
			$permissions = json_decode($group->first()->permissions, true);
			if($permissions[$key] == true) {
				return true;
			}
		}
		return false;
	}

	public function exists() {
		return(!empty($this->_data)) ? true : false;
	}

	public function logout() {

		$this->_db->delete('v_users_admin_session', array('user_id', '=', $this->data()->id));
		Session::delete($this->_sessionName);
		if (isset($_SESSION['facebook_access_token'])) {
			Session::delete('facebook_access_token');
		}
		Cookie::delete($this->_cookieName);
	}

	public function data() {
		return $this->_data;
	}

	public function isLoggedIn() {
		return $this->_isLoggedIn;
	}

	public function generate_new_password($email = null) {
		$random_str = Helpers::generateRandomString();
		$user = $this->find($email);
		$new_password = Hash::make($random_str, $this->data()->salt);
		$id = $this->data()->id;
		$fields = array(
			'password' => $new_password
		);
		if(!$this->_db->update('v_users_admin', $id, $fields)) {
			return false;
		} else {
			return $random_str;
		}
	}

	public function activate_user($username, $activation_key) {
		$user = $this->find($username);
		if($user) {
			if($this->data()->activation_key === $activation_key) {
				if($this->_db->update('v_users_admin', $this->data()->id, array('activation_status' => 'checked'))) {
					return true;
				}
			}
		}
		return false;
	}

	public function get_all_users() {
		return $this->get_all_users_but_me();
	}

	public function name_by_id($id) {
		$data = $this->_db->get('v_users_admin', array('id', '=', $id));
		return $data->first()->name;
	}

	public function user_by_id($id) {
		$data = $this->_db->get('v_users_admin', array('id', '=', $id));
		return $data->first();
	}

	public function get_configuracion() {
		return $this->_db->query("SELECT * FROM v_configuracion LIMIT 1")->results()[0];
	}

	public function get_last_user() {
		return $this->_db->query("SELECT * FROM v_users_admin ORDER BY id DESC LIMIT 1")->results()[0];
	}

	public function update_configuracion($facebook,$twitter,$youtube) {
		return $this->_db->update('v_configuracion', 1, array(
			'facebook' => $facebook,
			'twitter' => $twitter,
			'youtube' => $youtube
		));
	}

	public function update_theme($theme) {
		return $this->_db->update('v_users_admin', $this->data()->id, array(
			'theme' => $theme
		));
	}

	public function search_users($query, $what = 'name') {
		$users = $this->get_all_users_but_me();
		$array = array();
		foreach ($users as $key) {
			$found = stripos($key->$what, $query);
			if($found!==false) {
				$array[]=$key;
			}
		}
		return $array;
	}

	public function get_all_users_but_me() {
		$users = $this->_db->query("SELECT * FROM v_users_admin")->results();
		$array = array();
		foreach ($users as $key) {
			if($key->id != $this->data()->id && $key->group!=1) {
				$array[]=$key;
			}
		}
		return $array;
	}

	public function get_latest_activity($id) {
		return $this->_db->getRows("SELECT * FROM v_activity WHERE user_from = '$id' ORDER BY id DESC");
	}

	public function check_invitation($email) {
		
	}

	public function update_budget($budget) {
		$current_budget = $this->data()->balance;
		$new_budget = $current_budget - $budget;
		$this->_db->update('v_users_admin', $this->data()->id, array('balance' => $new_budget));
	}

	public function change_image($file_name) {
		$this->_db->update('v_users_admin', $this->data()->id, array('img' => $file_name));
	}

	public function update_info($username, $email, $name, $id = 0) {
		if($id == 0) {
			$edit_id = $this->data()->id;
		} else {
			$edit_id = $id;
		}
		$this->_db->update('v_users_admin', $edit_id, array(
			'username' => Helpers::create_url($username),
			'email' => $email,
			'name' => $name
		));
	}

	public function update_position($position, $user_id) {
		$this->_db->update('v_users_admin', $user_id, array(
			'group' => intval($position)
		));
	}

	public function get_companies() {
		return $this->_db->query("SELECT * FROM v_companies ORDER BY id DESC")->results();
	}

	public function get_company($id) {
		return $this->_db->get('v_companies', array('id', '=', $id))->results()[0];
	}

	public function create_company($name, $file_name) {
		return $this->_db->insert('v_companies', array(
			'name' => $name,
			'img' => $file_name
		));
	}

	public function get_company_users($id) {
		return $this->_db->query("SELECT * FROM v_users_admin WHERE company = '$id'")->results();
	}

	public function search_companies($query) {
		$companies = $this->_db->query("SELECT * FROM v_companies ORDER BY id DESC")->results();
		$array = array();
		foreach ($companies as $key) {
			$found = stripos($key->name, $query);
			if($found!==false) {
				$array[]=$key;
			}
		}
		return $array;
	}

	public function get_last_company() {
		return $this->_db->query("SELECT * FROM v_companies ORDER BY id DESC LIMIT 1")->results()[0];
	}

	public function update_company_img($file_name, $company_id) {
		$this->_db->update('v_companies', $company_id, array('img' => $file_name));
	}

	public function update_company_name($name, $company_id) {
		$this->_db->update('v_companies', $company_id, array('name' => $name));
	}

	public function update_user_img($file_name, $user_id = 0) {
		if($user_id == 0) {
			$id = $this->data()->id;
		} else {
			$id = $user_id;
		}
		$this->_db->update('v_users_admin', $id, array('img' => $file_name));
	}

	public function delete_company($company_id) {
		$this->_db->delete('v_companies', array('id', '=', $company_id));
	}

	public function delete_user($user_id) {
		$this->_db->delete('v_users_admin', array('id', '=', $user_id));
	}

	public function update_info_with_position($username, $email, $name, $position, $id) {
		$this->_db->update('v_users_admin', $id, array(
			'username' => Helpers::create_url($username),
			'email' => $email,
			'name' => $name,
			'group' => intval($position)
		));
	}

}















