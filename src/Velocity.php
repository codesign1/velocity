<?php

namespace Velocity;

use Velocity\Db\Db;
use Velocity\Helpers\Helpers;
use Velocity\Helpers\Timedate;
use Velocity\Helpers\Redirect;
use Velocity\Config\Config;
use Velocity\Authentication\Session;
use Velocity\Authentication\Validate;
use Velocity\Authentication\Cookie;
use Velocity\Authentication\Input;
use Velocity\Authentication\Token;
use Velocity\Security\Hash;
use Velocity\Security\Encrypt;
use Velocity\Users\User;
use Velocity\Users\User_admin;
use Velocity\Cms\Cms;
use Velocity\Ecommerce\Shop;
use Velocity\Files\Files;
use Velocity\Email\PHPMailer;
use Velocity\Email\SMTP;
use Velocity\Core\Router;

use Twig_Environment;
use Twig_Loader_Filesystem;

class Velocity {
    private static $instance;

    public $twig;

    public $user_app_directory;

    public $application_name;

    public static function getInstance($app_name) {
        if (static::$instance === null) {
            static::$instance = new static($app_name);
        }
        return static::$instance;
    }

    protected function __construct($app_name, $enable_cache = false, $cache_ignore_pages = null,
                                   $default_timezone = 'America/Bogota') {
        // initialize php sessions
        session_start();

        // check the cache, in case we can save a lot of time
        $this->set_up_cache_settings($enable_cache, $cache_ignore_pages);
        $this->check_cache();

        // save the app name and get the directory from where velocity was called from
        $this->application_name = $app_name;
        $this->user_app_directory = getcwd();

        // takes the user config file and stores is into the global config
        $this->set_up_app_config();

        ob_start('ob_gzhandler'); //Turn on output buffering, "ob_gzhandler" for the compressed page with gzip.

        // set the default timezone
        date_default_timezone_set($default_timezone);

        $this->set_up_twig();
    }

    private function set_up_app_config() {
        $config_file_url = $this->user_app_directory . '/config/config.json';
        if (!file_exists($config_file_url)) {
            echo('<b>Error!</b> Config file not present!');
            exit();
        } else {
            // get the config file contents
            $config_file_contents = file_get_contents($config_file_url);
            $decoded_config_data = json_decode($config_file_contents, true);
            
            // store it into the global config variable so that the Config class can
            // use it
            $GLOBALS['config'] = $decoded_config_data;
        }
    }

    private function check_cache() {
        // requested dynamic page (full url)
        $dynamic_url = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . $_SERVER['QUERY_STRING'];
        $cache_file = $this->cache_folder . md5($dynamic_url) . $this->cache_extension; // construct a cache file
        $cache_ignore = (in_array($dynamic_url, $this->cache_ignore_pages)) ? true : false; //check if url is in ignore list

        //check Cache exist and it's not expired.
        if (!$cache_ignore && $this->cache_on && file_exists($cache_file) &&
            time() - $this->cache_time < filemtime($cache_file)
        ) {
            readfile($cache_file); //read Cache file
            ob_end_flush(); //Flush and turn off output buffering
            exit(); //no need to proceed further, exit the flow.
        }
    }

    private function set_up_cache_settings($enable_cache, $cache_ignore_pages) {
        $this->cache_on = $enable_cache;
        $this->cache_extension = '.php'; //file extension
        $this->cache_time = 3600;  //Cache file expires afere these seconds (1 hour = 3600 sec)
        $this->cache_folder = dirname(__FILE__) . '/' . 'cache/';
        $this->cache_ignore_pages = $cache_ignore_pages;
    }

    private function set_up_twig() {
        $twig_loader = new Twig_Loader_Filesystem($this->user_app_directory . "/app/templates/");
        $this->twig = new Twig_Environment($twig_loader, array(
            'cache' => false,
            'debug' => true
        ));

        // add twig global variables
        $this->twig->addGlobal('_APP_NAME', $this->application_name);
        $this->twig->addGlobal('_ASSETS_LOCATION', 'http://' . $_SERVER['HTTP_HOST'] . '/public');
    }

    private function execute_routing() {
        // execute the user's routing commands and then render
        $user_router_config = $this->application_name . '\\app\\RouterConfig';
        $user_router_config::execute();
        
        Router::init($this); // passing the velocity instance
        Router::route($_SERVER['REQUEST_METHOD'], $_SERVER['REQUEST_URI']);
    }

    private function authenticate() {
        // CHECK IF USER LOGGED IN WITH COOKIES
        // if(Cookie::exists(Config::get('remember/cookie_name')) && !Session::exists(Config::get('session/session_name'))) {
        // 	$hash = Cookie::get(Config::get('remember/cookie_name'));
        // 	$hasCheck = Db::getInstance()->get('v_users_session', array('hash', '=', $hash));
        // 	if($hasCheck->count()) {
        // 		$user = new User($hasCheck->first()->user_id);
        // 		$user->login();
        // 	}
        // }
    }

    public function add_twig_global($name, $value) {
        $this->twig->addGlobal($name, $value);
    }

    public function add_twig_globals($array) {
        foreach($array as $key => $value) {
            $this->twig->addGlobal($key, $value);
        }
    }

    public function execute() {
        $this->execute_routing();
    }
}
