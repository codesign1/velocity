<?php

namespace Velocity\Ecommerce;

use Velocity\Velocity;
use Velocity\Core\Controller;
use Velocity\Ecommerce\Shop;
use Velocity\Users\User;

/**
 * Holds attributes common to all controllers in the app, the constructor function and
 * a render function that prints the template to the output using Twig
 */
class CartController extends Controller {

	public  $cart,
			$cart_total,
			$user,
			$user_data,
			$shop,
			$categorias,
			$cats,
			$subcats,
			$isLoggedIn,
			$direcciones,
			$pedidos;

	public function __construct($name,  $description,  $keywords,  $author) {
		parent::__construct($name,  $description,  $keywords,  $author);
		
		$this->user = new User();
		if($this->user->isLoggedIn()) {
			$this->isLoggedIn = true;
			$this->user_data = $this->user->data();
			$this->shop = new Shop($this->user_data->id);
			$this->direcciones = $this->shop->direcciones();
			$this->pedidos = $this->shop->get_pedidos_master($this->user_data->email);
		} else {
			$this->isLoggedIn = false;
			$this->shop = new Shop();
		}

		list($this->cart, $this->cart_total) = $this->shop->get_cart();

		$this->categorias = $this->shop->get_categorias();

		$this->cats = array();
		$this->subcats = array();

		foreach ($this->categorias as $key) {
			if($key->tipo == 'categoria') {
				$this->cats[] = $key;
			} else {
				$this->subcats[$key->parent_url][] = $key;
			}
		}

	}
}
