<?php
/**
 * Velocity Database Class - velocity-database-class.php
 * PHP Version 5 and +
 * @package v.db.class.php
 * @link https://velocity-framework.com/php/db.php
 * @author Juan Pablo Casabianca <jpc@pixelstudio32.com>
 * @copyright 2014 Juan Pablo Casabianca, GRUPO CASABIANCA IREGUI SAS
 * @copyright 2014 PHPACADEMY - As learned in there tutorial
 * @license http://www.http://creativecommons.org/licenses/by-nc-nd/4.0/
 * Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)
 */

namespace Velocity\Ecommerce;

use Velocity\Db\Db;
use Velocity\Helpers\Timedate;
use Velocity\Helpers\Helpers;

class Shop {

	protected $db,
			$user_id,
			$logged_in,
			$cart;

	public function __construct($user = '') {
		$this->db = Db::getInstance();
		if($user!='') {
			$this->user_id = $user;
			$this->logged_in = true;
		}
	}

	public function create($table, $fields) {
		return $this->db->insert($table, $fields);
	}

	public function update($table, $id, $fields) {
		return $this->db->update($table, $id, $fields);
	}

	public function get($table, $fields) {
		return $this->db->get($table, $fields);
	}

	public function query($query) {
		return $this->db->query($query)->results();
	}

	public function delete($table, $fields) {
		return $this->db->delete($table, $fields);
	}

	public function get_product($value, $identifier = 'id') {
		return $this->db->get('v_productos', array($identifier, '=', $value))->first();
	}

	public function get_combo($value, $identifier = 'id') {
		return $this->db->get('v_productos_complejos', array($identifier, '=', $value))->first();
	}

	public function get_combo_name($value, $identifier = 'id') {
		return $this->db->get('v_productos_complejos', array($identifier, '=', $value))->first()->nombre;
	}

	public function get_productos($filter, $what = 'happy') {
		if($filter=='todos') {
			return $this->db->get('v_productos', array('tipo', '=', $what))->results();
		} elseif ($filter=='combos') {
			return $this->db->query("SELECT * FROM v_productos_complejos ORDER BY id DESC")->results();
		} else {
			return $this->db->query("SELECT * FROM v_productos WHERE tipo = '$what' AND categoria = '$filter' ORDER BY id DESC")->results();
		}
	}

	public function check_if_in_cart($sku) {
		$user_id = $this->user_id;
		return $this->db->query("SELECT * FROM v_cart WHERE user_id = '$user_id' AND sku = '$sku' ORDER BY id DESC")->results();
	}

	public function get_cart_user() {
		$user_id = $this->user_id;
		return $this->db->get('v_cart', array('user_id', '=', $user_id))->results();
	}

	public function add_to_cart($sku, $modelo = 0, $cant = 1) {

		$product = $this->get_product($sku, 'sku');
		
		if($this->logged_in && !isset($_SESSION['cart'])) {

			$exists = $this->check_if_in_cart($sku);
			if(count($exists)) {

				$update_id = $exists[0]->id;
				$cant_nueva = $exists[0]->cantidad += $cant;

				$this->db->update('v_cart', $update_id, array(
					'cantidad' => $cant_nueva
				));

			} else {

				$this->db->insert('v_cart', array(
					'added' => Timedate::get_mysql_format(),
					'user_id' => $this->user_id,
					'sku' => $sku,
					'modelo' => $modelo,
					'nombre' => $product->nombre,
					'precio' => $product->precio,
					'cantidad' => $cant
				));

			}

		} else {
			$ref = $sku . '||' . $modelo;
			if(!isset($_SESSION['cart'])) {
				$_SESSION['cart'][$ref] = $cant;
			} else {	
				if(isset($_SESSION['cart'][$ref])) {
					if (($cant < 0 && $_SESSION['cart'][$ref] > 1) // don't subtract if amount 1 or less
                        || $cant >= 0) {
                        $_SESSION['cart'][$ref] += $cant;
                    }
				} else {
					$_SESSION['cart'][$ref] = $cant;
				}
			}
		}
	}

	public function get_cart() {
		if($this->logged_in && !isset($_SESSION['cart'])) {
			$cart_array = array();
			$total = 0;
			$cart_user = $this->get_cart_user();
			if(count($cart_user)) {
				foreach ($cart_user as $cart_u) {
					if(stripos($cart_u->sku, 'combo')!==false) {
						$prod = $this->get_combo($cart_u->sku, 'sku');
					} else {
						$prod = $this->get_product($cart_u->sku, 'sku');
					}
					$total += $cart_u->precio * $cart_u->cantidad;

					$cart_array[] = array(
						'nombre' => $prod->nombre,
						'sku' => $prod->sku,
						'precio' => $prod->precio,
						'cantidad' => $cart_u->cantidad
					);
				}
				return array($cart_array, $total);
			} else {
				return array(array(), 0);
			}
		} else {
			if(!isset($_SESSION['cart'])) {

				return array(array(), 0);

			} else {
				if(count($_SESSION['cart']) > 0) {

					$cart_array = array();
					$total = 0;

					foreach ($_SESSION['cart'] as $key => $value) {
						$refe = explode('||', $key);
						$sku_key = $refe[0];
						$modelo_key = $refe[1];
						$prod = $this->get_product($sku_key, 'sku');
						$total += $prod->precio * $value;
						$cart_array[] = array(
							'nombre' => $prod->nombre,
							'sku' => $prod->sku,
							'modelo' => $modelo_key,
							'precio' => $prod->precio,
							'cantidad' => $value
						);
					}

					return array($cart_array, $total);

				} else {
					return array(array(), 0);
				}
			}
		}
	}

	public function remove_from_cart($sku) {
		if($this->logged_in) {
			$user_id = $this->user_id;
			$this->db->query("DELETE FROM v_cart WHERE user_id = '$user_id' AND sku = '$sku'");
		} else {
			if(isset($_SESSION['cart'])) {
				unset($_SESSION['cart'][$sku]);
			}
		}
	}

	public function get_sub_productos($sku) {
		return $this->db->get('v_productos_complejos_sencillos', array('parent_sku', '=', $sku))->results();
	}

	public function hacer_pedido($unique_id, $datos, $envio, $descuento, $bono) {
		
		list($cart, $cart_total) = $this->get_cart();

		// fix, because default parameter values don't work
        $descuento = !is_null($descuento) ? $descuento : 1;
        $bono = !is_null($bono) ? $bono : 0;

		$tot = ($cart_total * $descuento) - $bono;
		if($tot < 0) {
			$cart_total = 0;
		} else {
			$cart_total = $tot;
		}

		$total_mas_envio = $cart_total + $envio;

		$this->db->insert('v_pedidos_master', array(
			'unique_id' => $unique_id,
			'ano' => date('Y'),
			'mes' => date('m'),
			'dia' => date('d'),
			'hora' => date('G:i'),
			'num_productos' => count($cart),
			'precio' => $cart_total,
			'envio' => $envio,
			'precio_mas_envio' => $total_mas_envio,
			'nombre' => $datos['nombre'],
			'email' => $datos['email'],
			'celular' => $datos['celular'],
			'direccion' => $datos['direccion'],
			'ciudad' => $datos['ciudad']
		));
		foreach ($cart as $cart_item) {
			$this->db->insert('v_pedidos', array(
				'unique_id' => $unique_id,
				'ano' => date('Y'),
				'mes' => date('m'),
				'dia' => date('d'),
				'hora' => date('G:i'),
				'nombre' => $cart_item['nombre'],
				'sku' => $cart_item['sku'],
				'modelo' => $cart_item['modelo'],
				'precio' => $cart_item['precio'],
				'cantidad' => $cart_item['cantidad']
			));
		}

		return $total_mas_envio;

	}

	public function payment_declined($unique_id){
		$pedido_id = $this->get_pedidos_master_id($unique_id);
		$status = $this->db->update('v_pedidos_master', $pedido_id, array(
			'status' => 'Rechazado'
		));
	}

	public function payed_order($unique_id) {
		$pedido_id = $this->get_pedidos_master_id($unique_id);
		$status = $this->db->update('v_pedidos_master', $pedido_id, array(
			'status' => 'Pagado'
		));
		if($status!==false) {
			$this->empty_cart();
			return true;
		} else {
			return false;
		}
	}

	public function check_order_type($unique_id) {
		$pedidos = $this->get_pedidos($unique_id);
		$vitabox = false;
		$combo = false;
		$typee = 'referencia';
		foreach ($pedidos as $key) {
			$try1 = stripos($key->sku, 'vitabox');
			$try2 = stripos($key->sku, 'combo');
			if($try1 !== false) {
				if($key->sku == 'vitaboxkids001' || $key->sku == 'vitaboxkids002' || $key->sku == 'vitaboxkids003') {
					$vitabox_type = 'kids';
				} elseif ($key->sku == 'vitaboxlight001' || $key->sku == 'vitaboxlight002' || $key->sku == 'vitaboxlight003') {
					$vitabox_type = 'light';
				} else {
					$vitabox_type = 'mix';
				}
				$vitabox = true;
				$typee = 'vitabox';
			}
			if ($try2 !== false) {
				$combo = true;
				$typee = 'combo';
				$name = $this->get_combo_name($key->sku, 'sku');
			}
		}
		if($vitabox === true) {
			return array($typee, $vitabox_type);
		} elseif ($combo === true) {
			return array($typee, $name);
		} else {
			return array($typee, $pedidos[0]->nombre);
		}
	}

	public function empty_cart() {
		if($this->logged_in) {
			$user_id = $this->user_id;
			$this->db->delete('v_cart', array('user_id', '=', $user_id));
		} else {
			if(isset($_SESSION['cart'])) {
				unset($_SESSION['cart']);
			}
		}
	}

	public function get_suscripciones() {
		return $this->db->get('v_pedidos_master', array('suscripcion', '=', 1))->results();
	}

	public function get_pedidos_master($email) {
		return $this->db->get('v_pedidos_master', array('email', '=', $email))->results();
	}

	public function get_pedido_master($unique_id) {
		return $this->db->get('v_pedidos_master', array('unique_id', '=', $unique_id))->first();
	}

	public function all_pedidos($filtro = 'todos'){
		if($filtro=='sin-entregar') {
			return $this->db->query("SELECT * FROM v_pedidos_master WHERE entregado = 0 ORDER BY id DESC")->results();
		} else if($filtro=='entregados') {
			return $this->db->query("SELECT * FROM v_pedidos_master WHERE entregado = 1 ORDER BY id DESC")->results();
		} else if($filtro=='guardados') {
			return $this->db->query("SELECT * FROM v_pedidos_master WHERE guardado = 1 ORDER BY id DESC")->results();
		} elseif ($filtro=='importantes') {
			return $this->db->query("SELECT * FROM v_pedidos_master WHERE guardado = 0 AND entregado = 0 ORDER BY id DESC")->results();
		} else {
			return $this->db->query("SELECT * FROM v_pedidos_master ORDER BY id DESC")->results();
		}
	}

	public function get_pedidos($unique_id) {
		return $this->db->get('v_pedidos', array('unique_id', '=', $unique_id))->results();
	}

	public function get_pedidos_master_id($unique_id) {
		return $this->db->get('v_pedidos_master', array('unique_id', '=', $unique_id))->first()->id;
	}

	public function get_sub_pedidos($unique_id) {
		return $this->db->get('v_pedidos', array('unique_id', '=', $unique_id))->results();
	}

	public function get_pedidos_totales_suscritos() {
		return $this->db->get('v_pedidos_master', array('suscripcion', '=', 1))->results();
	}

	public function pedidos_proximos_a_vencer() {
		$ti = strtotime(date("Y-m-d")) - 2332800;
		$pedidos = $this->get_pedidos_totales_suscritos();
		$return_array = array();
		foreach ($pedidos as $key) {		
			$ti2 = strtotime(date($key->ano.'-'.$key->mes.'-'.$key->dia));
			if($ti >= $ti2) {
				$return_array[] = $key;
			} 
		}
		return $return_array;
	}

	public function get_total_ventas() {
		$res = $this->db->query("SELECT precio FROM v_pedidos_master")->results();
		$total = 0;
		foreach ($res as $key) {
			$total += $key->precio;
		}
		return $total;
	}

	public function get_total_pedidos() {
		$res = $this->db->query("SELECT precio FROM v_pedidos_master")->results();
		return count($res);
	}

	public function get_total_ventas_hoy() {

		$date = time();
		$year = date('Y', $date);
		$month = date('m', $date); 
		$day = date('d', $date);

		$res = $this->db->query("SELECT precio FROM v_pedidos_master WHERE ano = '$year' AND mes = '$month' AND dia = '$day'")->results();
		$total = 0;
		foreach ($res as $key) {
			$total += $key->precio;
		}
		return $total;
	}

	public function get_total_pedidos_hoy() {
		$date = time();
		$year = date('Y', $date);
		$month = date('m', $date); 
		$day = date('d', $date);

		$res = $this->db->query("SELECT precio FROM v_pedidos_master WHERE ano = '$year' AND mes = '$month' AND dia = '$day'")->results();
		return count($res);
	}

	public function check_codigo($codigo) {
		$res = $this->db->query("SELECT* FROM v_promociones WHERE tipo = 'bono' AND codigo = '$codigo'")->first();
		$total = 1;
		if($res) {
			if($res->usado == 0) {
				$this->db->update('v_promociones', $res->id, array(
					'usado' => 1
				));
				return $total - $res->descuento;
			} else {
				return $total;	
			}
		} else {
			return $total;
		}
	}

	public function check_bono($bono) {
		$res = $this->db->query("SELECT* FROM v_promociones WHERE tipo = 'promocion' AND codigo = '$codigo'")->first();
		$total = 0;
		if($res) {
			if($res->usado == 0) {
				$this->db->update('v_promociones', $res->id, array(
					'usado' => 1
				));
				return $total + $res->plata;
			} else {
				return $total;	
			}
		} else {
			return $total;
		}
	}

	public function get_categorias($tipo = 'todos') {
		if($tipo=='categorias') {
			return $this->db->query("SELECT * FROM v_categorias WHERE tipo = 'categoria' ORDER BY nombre ASC")->results();
		} else {
			return $this->db->query("SELECT * FROM v_categorias ORDER BY nombre ASC")->results();
		}
	}	

	public function get_marcas(){
		return $this->db->query("SELECT * FROM v_categorias WHERE marca = 1")->results();
	}

	public function get_prod($cat = '', $subcat = '', $filtro = '') {
		if($cat == '' && $subcat == '' && $filtro == '') {

			$results = $this->db->query("SELECT * FROM v_productos ORDER BY id DESC")->results();

		} elseif ($cat != '' && $subcat == '' && $filtro == '') {

			if($cat=='todos') {
				$results = $this->db->query("SELECT * FROM v_productos ORDER BY id DESC")->results();
			} else {
				$results = $this->db->query("SELECT * FROM v_productos WHERE categoria = '$cat' ORDER BY id DESC")->results();
				if(count($results)==0) {
					$results = $this->db->query("SELECT * FROM v_productos WHERE marca = '$cat' ORDER BY id DESC")->results();
				}
			}

		} elseif ($cat != '' && $subcat != '' && $filtro == '') {

			if($subcat == 'todos') {
				$results = $this->db->query("SELECT * FROM v_productos WHERE categoria = '$cat' ORDER BY id DESC")->results();
			} else {
				$results = $this->db->query("SELECT * FROM v_productos WHERE categoria = '$cat' AND sub_categoria = '$subcat' ORDER BY id DESC")->results();
				if(count($results)==0) {
					$results = $this->db->query("SELECT * FROM v_productos WHERE marca = '$cat' ORDER BY id DESC")->results();
				}
			}

		} elseif ($cat != '' && $subcat != '' && $filtro != '') {

			if($cat=='todos' && $subcat == 'todos') {
				if($filtro=='' || $filtro == 'todos') {
					$results = $this->db->query("SELECT * FROM v_productos ORDER BY id DESC")->results();
				} elseif ($filtro=='populares') {
					$results = $this->db->query("SELECT * FROM v_productos ORDER BY compras DESC")->results();
				} else {
					$results = $this->db->query("SELECT * FROM v_productos ORDER BY precio ASC")->results();
				}
			} else {
				if($filtro=='' || $filtro == 'todos') {
					$results = $this->db->query("SELECT * FROM v_productos WHERE categoria = '$cat' AND sub_categoria = '$subcat' ORDER BY id DESC")->results();
					if(count($results)==0) {
						$results = $this->db->query("SELECT * FROM v_productos WHERE marca = '$cat' ORDER BY id DESC")->results();
					}
				} elseif ($filtro=='populares') {
					$results = $this->db->query("SELECT * FROM v_productos WHERE categoria = '$cat' AND sub_categoria = '$subcat' ORDER BY compras DESC")->results();
					if(count($results)==0) {
						$results = $this->db->query("SELECT * FROM v_productos WHERE marca = '$cat' ORDER BY compras DESC")->results();
					}
				} else {
					$results = $this->db->query("SELECT * FROM v_productos WHERE categoria = '$cat' AND sub_categoria = '$subcat' ORDER BY precio ASC")->results();
					if(count($results)==0) {
						$results = $this->db->query("SELECT * FROM v_productos WHERE marca = '$cat' ORDER BY precio ASC")->results();
					}
				}
			}

		}

		return $results;

	}

	public function get_modelo_producto($id){
		return $this->db->query("SELECT * FROM v_modelos WHERE id = $id")->results();
	}

	public function get_modelos() {
		return $this->db->query("SELECT * FROM v_modelos ORDER BY id DESC")->results();
	}

	public function get_modelos_sku($sku, $modelo = 0) {
		if($modelo==0) {
			return $this->db->get('v_modelos', array('sku', '=', $sku))->results();
		} else {
			return $this->db->query("SELECT * FROM v_modelos WHERE sku = '$sku' AND id = '$modelo' ORDER BY id DESC")->results();
		}
	}
	
	public function get_sub_categorias($parent_url) {
		return $this->db->get('v_categorias', array('parent_url', '=', $parent_url))->results();
	}

	public function cat_title($url) {
		return $this->db->get('v_categorias', array('url', '=', $url))->first();
	}

	public function direcciones() {
		$u = $this->user_id;
		return $this->db->get('v_direcciones', array('user_id', '=', $u))->results();
	}

	public function crear_direccion($ciudad, $barrio, $direccion){
		return $this->db->insert('v_direcciones', array(
			'user_id' => $this->user_id,
			'direccion' => $direccion,
			'barrio' => $barrio,
			'ciudad' => $ciudad
		));
	}

	public function eliminar_direccion($id) {
        if ($this->logged_in) {
            $this->db->query("DELETE FROM v_direcciones WHERE id = " . $id);
        }
    }

	public function get_productos_relacionados($sku) {
		$producto = $this->get_product($sku, 'sku');
		$results = $this->db->query("SELECT * FROM v_productos ORDER BY id DESC LIMIT 9")->results();
		$array = array();
		foreach ($results as $result) {
			if($result->sku != $sku && count($array) < 8) {
				$array[]=$result;
			}
		}
		return $array;
	}

	public function get_productos_featured($limit = 8){
		return $this->db->query("SELECT * FROM v_productos WHERE featured = 1 ORDER BY id DESC LIMIT $limit")->results();
	}

	public function get_categoria($value, $identifier = 'url'){
		return $this->db->get('v_categorias', array($identifier, '=', $value))->first();
	}

	public function search_products($query) {
		$pros = $this->db->query("SELECT * FROM v_productos ORDER BY id DESC")->results();
		$results = array();
		foreach ($pros as $key) {
			$try1 = stripos($key->nombre, $query);
			$try2 = stripos($key->categoria, $query);
			$try3 = stripos($key->sub_categoria, $query);
			$try4 = stripos($key->marca, $query);
			$try5 = stripos($key->opcion, $query);
			$try6 = stripos($key->sku, $query);
			if($try1 !== false || $try2 !== false || $try3 !== false || $try4 !== false || $try5 !== false || $try6 !== false) {
				$results[] = $key;
			}
		}
		return $results;
	}

	public function get_chart_dates($time = 'ano'){

		$date = time();		
		$year = date('Y', $date);
		$month = date('n', $date); 
		$day = date('d', $date);

		if($time=='ano') {

			for ($i=1; $i <= 12; $i++) { 
				$dates_array[] = $year . '-' . $i;
			}
			return $dates_array;

		} elseif($time=='mes') {

			$num_days = date('t', $date);
			for ($i=1; $i <= $num_days; $i++) { 
				$dates_array[] = $year . '-' . $month . '-' . $i;
			}
			return $dates_array;

		}

	}

	public function get_integrantes(){
		return $this->db->query("SELECT * FROM v_equipo ORDER BY nombre ASC")->results();
	}

	public function get_portafolio(){
		return $this->db->query("SELECT * FROM v_portafolio ORDER BY id desc")->results();
	}

	public function get_important_users(){
		return $this->db->query("SELECT * FROM v_users WHERE group = 0 OR group = 1")->results();
	}
	public function add_to_favorites($sku) {
        if ($this->logged_in) {
            $this->db->insert('v_favorites', array(
                'sku' => $sku,
                'user_id' => $this->user_id
            ));
        }
    }

    public function remove_from_favorites($sku) {
        if ($this->logged_in) {
            $this->db->query("DELETE FROM v_favorites WHERE sku = '" . $sku . "' and user_id = " . $this->user_id);
        }
    }

    public function get_favorites() {
        if ($this->logged_in) {
            return $this->db->query("SELECT * FROM v_productos where sku in (select sku from v_favorites where user_id = "
                . $this->user_id . ")")->results();
        }
    }

    public function is_user_favorite($sku) {
        if($this->logged_in) {
            $res = $this->db->query("SELECT * FROM v_favorites WHERE sku = '" . $sku . "' and user_id = " . $this->user_id)->results();
            if(count($res) < 1) {
                return false;
            } else {
                return true;
            }
        }
    }

    public function use_database($db_name) {
        $this->db->query("use $db_name");
    }
}









