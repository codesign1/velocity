<?php
/**
 * Velocity Validate Class - velocity-validation-class.php
 * PHP Version 5 and +
 * @package v.validate.class.php
 * @link https://velocity-framework.com/php/validate.php
 * @author Juan Pablo Casabianca <jpc@pixelstudio32.com>
 * @copyright 2014 Juan Pablo Casabianca, GRUPO CASABIANCA IREGUI SAS
 * @copyright 2014 PHPACADEMY - As learned in there tutorial
 * @license http://www.http://creativecommons.org/licenses/by-nc-nd/4.0/
 * Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)
 */

namespace Velocity\Authentication;

use Velocity\Db\db;
use Velocity\Helpers\Helpers;

class Validate {

	private $_passed = false,
			$_errors = array(),
			$_db = null;

	public function __construct() {
		$this->_db = Db::getInstance();
	}

	public function check($source, $items = array()) {
		foreach ($items as $item => $rules) {
			foreach ($rules as $rule => $rule_value) {
				if(empty($source[$item]) && $item=="checkbox") {
					$this->addError("You must choose at least 1 {$item}.");
				} else {
					if(is_array($source[$item])) {
						$value_array = $source[$item];
						switch ($rule) {
							case 'checkedAtLeast':
								if(count($value_array)<$rule_value) {
									$this->addError("{$item} has to have at least {$rule_value} checked.");
								}
							break;
						}
					} else {
						$value = trim($source[$item]);
						$item = Helpers::escape($item);
						if($rule === 'required' && empty($value)) {
							$this->addError("{$item} is required");
						} else if (!empty($value)) {
							switch($rule) {
								case 'min':
									if(strlen($value) < $rule_value) {
										$this->addError("{$item} must be a minimum of {$rule_value} characters.");
									}
								break;
								case 'max':
									if(strlen($value) > $rule_value) {
										$this->addError("{$item} must be a maximum of {$rule_value} characters.");
									}
								break;
								case 'matches':
									if($value != $source[$rule_value]) {
										$this->addError("{$rule_value} must match {$item}");
									}
								break;
								case 'unique':
									$check = $this->_db->get($rule_value, array($item, '=', $value));
									if($check->count()) {
										$this->addError("{$item} already exists.");
									}
								break;
								case 'email':
									if(!filter_var($value, FILTER_VALIDATE_EMAIL)) {
										$this->addError("{$item} must be a valid email");
									}
								break;
								case 'thisOrThat':
									if($source[$rule_value]!="") {
										$this->addError("You chose both options: {$value}. Please choose only one!");
									}
								break;
								case 'lessThan':
									if($value > $rule_value) {
										$this->addError("You must currenlty have: {$value} Points. Please Consider Adding More to your $item");
									}
								break;
								case 'url':
									if (!filter_var($value, FILTER_VALIDATE_URL)) { 
			  							$this->addError("{$item} debe ser una url válida. (Debe contender http://)");
									} 
								break;
							}
						}
					}
				}
			}
		}

		if(empty($this->_errors)) {
			$this->_passed = true;
		}

		return $this;
	}


	public static function check_if_email($input) {
		return filter_var($input, FILTER_VALIDATE_EMAIL);
	}

	public static function url($url) {
		if(filter_var($url, FILTER_VALIDATE_URL)) {
			return true;
		} else {
			return false;
		}
	}

	private function addError($error) {
		$this->_errors[] = $error;
	}

	public function errors() {
		return $this->_errors;
	}

	public function passed() {
		return $this->_passed;
	}

}