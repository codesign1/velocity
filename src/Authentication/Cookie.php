<?php 
/**
 * Velocity Cookies Class - velocity-configuration-class.php
 * PHP Version 5 and +
 * @package v.cookies.class.php
 * @link https://velocity-framework.com/php/cookie.php
 * @author Juan Pablo Casabianca <jpc@pixelstudio32.com>
  * @copyright 2014 Juan Pablo Casabianca, GRUPO CASABIANCA IREGUI SAS
 * @copyright 2014 PHPACADEMY - As learned in there tutorial
 * @license http://www.http://creativecommons.org/licenses/by-nc-nd/4.0/
 * Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)
 */

namespace Velocity\Authentication;

class Cookie {

	public static function exists($name) {
		return (isset($_COOKIE[$name])) ? true : false;
	}

	public static function get($name) {
		return $_COOKIE[$name];
	}

	public static function put($name, $value, $expiry) {
		if(setcookie($name, $value, time() + $expiry, '/')) {
			return true;
		}
		return false;
	}


	public static function delete($name) {
		self::put($name, '', time() - 1);
	}

}