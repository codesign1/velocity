<?php 
/**
 * Velocity Configuration Class - velocity-input-class.php
 * PHP Version 5 and +
 * @package v.input.class.php
 * @link https://velocity-framework.com/php/input.php
 * @author Juan Pablo Casabianca <jpc@pixelstudio32.com>
 * @copyright 2014 Juan Pablo Casabianca, GRUPO CASABIANCA IREGUI SAS
 * @copyright 2014 PHPACADEMY - As learned in there tutorial
 * @license http://www.http://creativecommons.org/licenses/by-nc-nd/4.0/
 * Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)
 */

namespace Velocity\Authentication;

class Input {
	public static function exists($type = 'post') {
		switch($type) {
			case 'post':
				return(!empty($_POST)) ? true : false;
			break;
			case 'get':
				return(!empty($_GET)) ? true : false;
			break;
			default:
				return false;
			break;
		}
	}

	public static function get($item) {
		if(isset($_POST[$item])) {
			return $_POST[$item];
		} elseif(isset($_GET[$item])) {
			return $_GET[$item];
		}
		return "";
	}

	public static function get_santized($item) {
		$item = Input::get($item);
		return strtolower(stripslashes(trim(htmlspecialchars($item))));
	}
}