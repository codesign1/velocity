<?php
/**
 * Velocity Database Class - velocity-database-class.php
 * PHP Version 5 and +
 * @package v.db.class.php
 * @link https://velocity-framework.com/php/db.php
 * @author Juan Pablo Casabianca <jpc@pixelstudio32.com>
 * @copyright 2014 Juan Pablo Casabianca, GRUPO CASABIANCA IREGUI SAS
 * @copyright 2014 PHPACADEMY - As learned in there tutorial
 * @license http://www.http://creativecommons.org/licenses/by-nc-nd/4.0/
 * Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)
 */

namespace Velocity\Files;

use Velocity\Db\Db;

class Files {

	private $db,
			$user_id;

	public  $max_file_size,
			$allowed_extensions;

	public function __construct($user = '', $max_file_size = 20000000, $allowed_extensions = array('jpg','jpeg','png')) {
		$this->db = Db::getInstance();
		$this->max_file_size = $max_file_size;
		$this->allowed_extensions = array(" ", "c","m","7z","ai","cs","db","gz","js","pl","ps","py","rm","ra","3dm","3g2","3gp","8bi","aif","app","asf","asx","avi","bak","bat","bin","bmp","cab","cer","cfg","cgi","com","cpl","cpp","dbf","dbx","deb","dll","dmg","dmp","doc","csr","css","csv","cur","dat","drv","drw","dtd","dwg","dxf","efx","eps","exe","fla","flv","fnt","fon","gam","gho","gif","gpx","hqx","iff","ini","iso","jar","jpg","m3u","m4a","max","mdb","mid","mim","mov","mp3","mp4","mpa","mpg","msg","msi","nes","ori","otf","jsp","key","kml","lnk","log","pct","pdb","pdf","pif","pkg","png","pps","ppt","prf","psd","qxd","qxp","rar","rels","rom","rpm","rss","rtf","sav","sdf","sit","sql","svg","swf","sys","thm","tif","tmp","ttf","txt","uue","vb","vcd","vcf","vob","wav","wks","wma","wmv","wpd","wps","wsf","xll","xls","xml","yuv","zip","docx","indd","java","part","pptx","sitx","zipx","xlsx","pages","accdb","class","toast","plugin","gadget","targz","torrent","keychain","pspimage", "php", "html", "css", "scss", "js");
		if($user!='') {
			$this->user_id = $user;
		}
	}

	public function set_allowed_extension($allowed) {
		$this->allowed_extensions = $allowed;
	}

	public function upload_exists($name) {
		if(!empty($_FILES[$name]['name'])) {
			return true;
		} else {
			return false;
		}
	}

	public function upload($name, $folder, $new_file_name = 'No') {

		$file = $_FILES[$name];

		// Set File Properties
		$file_name = $file['name'];
		$file_tmp = $file['tmp_name'];
		$file_size = $file['size'];
		$file_error = $file['error'];

		// Work Out File Extension
		$file_ext = explode('.', $file_name);
		$file_ext = strtolower(end($file_ext));

		$allowed = array('txt', 'jpg', 'pdf');

		if(in_array($file_ext, $this->allowed_extensions)) {
			if($file_error === 0) {
				if($file_size <= $this->max_file_size) {

					if($new_file_name == 'No') {
						$file_destination = $folder . $file_name;
					} else {
						$file_name = $new_file_name . '.' . $file_ext;
						$file_destination = $folder . uniqid() . uniqid() . '.' . $file_ext;;
					}
					
					if(move_uploaded_file($file_tmp, $file_destination)) {
						return array(true, $file_destination);
					} else {
						return array('Could not upoad file', $file_name);
					}
				} else {
					return array('File size exceeds maximum file size permitted', $file_name);
				}
			} else {
				return array('File has an error', $file_name);
			}
		} else {
			return array('File Extension not allowed', $file_name);
		}
	}

	public function exists_multiple($name) {
		if(!empty($_FILES[$name]['name'][0])) {
			return true;
		} else {
			return false;
		}
	}

	public function multiple_uploads($name, $folder, $new_file_name = 'No') {
		$files = $_FILES[$name];
		$uploaded = array();
		$failed = array();
		foreach ($files['name'] as $position => $file_name) {
			$file_tmp = $files['tmp_name'][$position];
			$file_size = $files['size'][$position];
			$file_error = $files['error'][$position];
			$file_ext = explode('.', $file_name);
			$file_ext = strtolower(end($file_ext));
			if(in_array($file_ext, $this->allowed_extensions)) {
				if($file_error === 0) {
					if(intval($file_size) <= $this->max_file_size) {
						if($new_file_name == 'No') {
							$file_destination = $folder . $file_name;
						} else {
							$file_destination = $folder . uniqid() . uniqid() . '.' . $file_ext;
						}
						if(move_uploaded_file($file_tmp, $file_destination)) {
							$uploaded[$position] = $file_destination;
						}
					} else {
						$failed[$position] = "$file_name size larger than maximum size 2MB";
					}
				} else {
					$failed[$position] = "$file_name error with code $file_error";
				}
			} else {
				$failed[$position] = "$file_name file extension '$file_ext' not permitted";
			}
		}
		return array($uploaded, $failed);
	}

	public function multiple_uploads_with_names($name, $folder, $new_file_name = 'No') {
		$files = $_FILES[$name];
		$uploaded = array();
		$failed = array();
		$file_destination_name = array();
		foreach ($files['name'] as $position => $file_name) {
			$file_tmp = $files['tmp_name'][$position];
			$file_size = $files['size'][$position];
			$file_error = $files['error'][$position];
			$file_ext = explode('.', $file_name);
			$file_ext = strtolower(end($file_ext));
			if(in_array($file_ext, $this->allowed_extensions)) {
				if($file_error === 0) {
					if($file_size <= $this->max_file_size) {
						$file_destination = $folder . uniqid() . uniqid() . '.' . $file_ext;
						$file_destination_name[] = $file_name;
						if(move_uploaded_file($file_tmp, $file_destination)) {
							$uploaded[$position] = $file_destination;
						}
					} else {
						$failed[$position] = "$file_name size larger than maximum size 2MB";
					}
				} else {
					$failed[$position] = "$file_name error with code $file_error";
				}
			} else {
				$failed[$position] = "$file_name file extension '$file_ext' not permitted";
			}
		}
		return array($uploaded, $failed, $file_destination_name);
	}

	public function get_user_files($user_id) {

	}

	public function get_file_extension($file_name) {
		$file_ext = explode('.', $file_name);
		if(count($file_ext)==1) {
			return 'folder';
		} else {
			$file_ext = strtolower(end($file_ext));
			return $file_ext;
		}
	}

	public function get_file_icon($file_name) {

		$file_ext = $this->get_file_extension($file_name);
		// if($file_ext == 'doc' || $file_ext == 'docx') {
		// 	return '<span class="icon-file-word-o"></span>';	
		// } elseif ($file_ext == 'pdf') {
		// 	return '<span class="icon-file-pdf-o"></span>';
		// } elseif ($file_ext == 'pptx') {
		// 	return '<span class="icon-file-powerpoint-o"></span>';
		// } elseif ($file_ext == 'xlsx' || $file_ext == 'xls' || $file_ext == 'csv') {
		// 	return '<span class="icon-file-excel-o"></span>';
		// } elseif ($file_ext == 'png' || $file_ext == 'jpg' || $file_ext == 'jpeg' || $file_ext == 'gif') { 
		// 	return '<span class="icon-file-image-o"></span>';
		// } elseif ($file_ext == 'zip') { 
		// 	return '<span class="icon-file-archive-o"></span>';
		// } elseif ($file_ext == 'mp3' || $file_ext == 'wma' || $file_ext == 'aac' || $file_ext == 'm3u') {
		// 	return '<span class="icon-file-audio-o"></span>';
		// } elseif ($file_ext == 'mp4' || $file_ext == 'mov' ) {
		// 	return '<span class="icon-file-movie-o"></span>';
		// } else
		if ($file_ext == 'folder') {
			return '<span class="icon-folder"></span>'; 
		} elseif ($file_ext == 'png' || $file_ext == 'jpg' || $file_ext == 'jpeg' || $file_ext == 'gif') {
			return 'image';
		} else {
			return '<span class="icon-file2"></span>';
		}
	}

	function format_size_units($bytes) {
        if ($bytes >= 1073741824) {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        } elseif ($bytes >= 1048576) {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        } elseif ($bytes >= 1024) {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        } elseif ($bytes > 1) {
            $bytes = $bytes . ' bytes';
        } elseif ($bytes == 1) {
            $bytes = $bytes . ' byte';
        } else {
            $bytes = '0 bytes';
        }
        return $bytes;
	}

	public function user_can_see($user_id, $file_url) {
		$file = $this->db->query("SELECT * FROM v_files WHERE owner = '$user_id' AND url = '$file_url'")->results();
		if(count($file)>0) {
			return $file[0];
		} else {
			$file = $this->db->query("SELECT * FROM v_files WHERE url = '$file_url'")->results();
			if(count($file)>0) {
				if($file[0]->status == 'shared') {
					$shared_people = explode('||', $file[0]->shared);
					if(in_array($this->user_id, $shared_people)) {
						return true;
					} else {
						return false;
					}
				} else {
					return false;
				}
			} else {
				return false;
			}		
		}
	}

	public function upload_files_to_db($uploaded, $directory) {
		foreach ($uploaded as $url) {
			$type = $this->get_file_name($url);
			if($type != 'folder') {
				$type = 'file';
			}
			$this->db->insert('v_files', array(
				'url' => $url,
				'directory' => $directory,
				'type' => $type,
				'owner' => $this->user_id
			));
		}
	}

	public function upload_to_db($url, $directory) {
		$type = $this->get_file_name($url);
		if($type != 'folder') {
			$type = 'file';
		}
		$this->db->insert('v_files', array(
			'url' => $url,
			'directory' => $directory,
			'type' => $type,
			'owner' => $this->user_id
		));
	}

	public function upload_files_to_db_tareas($uploaded, $tarea_id, $names) {
		for ($i=0; $i < count($uploaded); $i++) { 
			$this->db->insert('v_tareas_archivos', array(
				'logged' => Timedate::get_mysql_format(),
				'tarea_id' => $tarea_id,
				'name' => $names[$i],
				'url' => $uploaded[$i],
				'user_id' => $this->user_id
			));
		}
	}

	public function set_allowed_types() {
		return array(" ", "c","m","7z","ai","cs","db","gz","js","pl","ps","py","rm","ra","3dm","3g2","3gp","8bi","aif","app","asf","asx","avi","bak","bat","bin","bmp","cab","cer","cfg","cgi","com","cpl","cpp","dbf","dbx","deb","dll","dmg","dmp","doc","csr","css","csv","cur","dat","drv","drw","dtd","dwg","dxf","efx","eps","exe","fla","flv","fnt","fon","gam","gho","gif","gpx","hqx","iff","ini","iso","jar","jpg","m3u","m4a","max","mdb","mid","mim","mov","mp3","mp4","mpa","mpg","msg","msi","nes","ori","otf","jsp","key","kml","lnk","log","pct","pdb","pdf","pif","pkg","png","pps","ppt","prf","psd","qxd","qxp","rar","rels","rom","rpm","rss","rtf","sav","sdf","sit","sql","svg","swf","sys","thm","tif","tmp","ttf","txt","uue","vb","vcd","vcf","vob","wav","wks","wma","wmv","wpd","wps","wsf","xll","xls","xml","yuv","zip","docx","indd","java","part","pptx","sitx","zipx","xlsx","pages","accdb","class","toast","plugin","gadget","targz","torrent","keychain","pspimage", "php", "html", "css", "scss", "js");
	}

	public function create_folder($directory, $folder_name) {
		$url = $directory . $folder_name;
		$status = $this->db->insert('v_files', array(
			'url' => $url,
			'type' => 'folder',
			'owner' => $this->user_id
		));
		if($status) {
			mkdir($url);
			return true;
		} else {
			return false;
		}
	}

	public function copy_files($ids) {
		$user_id = $this->user_id;
		$files_pending = $this->db->query("SELECT * FROM v_files_copy WHERE user_id = '$user_id' AND status = 'pending'")->results();
		if(count($files_pending)>0) {
			foreach ($files_pending as $file_pending) {
				$this->db->update('v_files_copy', $file_pending->id, array(
					'status' => 'cleared'
				));
			}
		}
		foreach ($ids as $file_id) {
			$this->db->insert('v_files_copy', array(
				'user_id' => $user_id,
				'file_id' => $file_id
			));
		}
	}

	public function get_file_name($url) {
		$file_parts = explode('/', $url);
		$end = end($file_parts);
		$parts = explode('.', $end);
		if(count($parts)>0) {
			return $end;
		} else {
			return 'folder';
		}
	}

	public function paste_files($directory) {
		$user_id = $this->user_id;
		$files_pending = $this->db->query("SELECT * FROM v_files_copy WHERE user_id = '$user_id' AND status = 'pending'")->results();
		if(count($files_pending)>0) {
			foreach ($files_pending as $file) {
				$file_id = $file->file_id;
				$file_to_copy = $this->db->query("SELECT * FROM v_files WHERE id = '$file_id'")->first();
				$file_name = $this->get_file_name($file_to_copy->url);
				if($file_name!='folder') {
					$new_file_name = $directory . $file_name;
					copy($file_to_copy->url, $new_file_name);
					$this->upload_files_to_db(array($new_file_name));
				}
				
			}
		}
	}

	public function create_zip($files = array(),$destination = '',$overwrite = false) {
		if(file_exists($destination) && !$overwrite) { return false; }
		$valid_files = array();
		if(is_array($files)) {
			foreach($files as $file) {
				if(file_exists($file)) {
					$valid_files[] = $file;
				}
			}
		}
		if(count($valid_files)) {
			$zip = new ZipArchive();
			if($zip->open($destination,$overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
				return false;
			}
			foreach($valid_files as $file) {
				$zip->addFile($file,$file);
			}
			$zip->close();
			$this->upload_files_to_db(array($destination));
			return file_exists($destination);
		} else {
			return false;
		}
	}

	public function publish_file($file_url) {
		$file = $this->get_file_id($file_url);
		$this->db->update('v_files', $file->id, array(
			'status' => 'public'
		));
		return $file;
	}

	public function get_file_id($url) {
		return $this->db->query("SELECT * FROM v_files WHERE url = '$url'")->first();
	}

	public function get_urls_from_ids($ids) {
		$ids_array = explode(',', $ids);
		$array = array();
		foreach ($ids_array as $key) {
			$array[] = $this->db->query("SELECT * FROM v_files WHERE id = '$key'")->first()->url;
		}
		return $array;
	}

	public function search_files($query) {
		$all_files = $this->db->query("SELECT * FROM v_files")->results();
		$found_files = array();
		foreach ($all_files as $file) {
			$try1 = stripos($file->url, $query);
			if($try1!==false) {
				$found_files[]=$file;
			}
		}
		return $found_files;
	}

	public function share_files($integrantesproj, $archivosacomp) {
		$files = explode(',', $archivosacomp);
		foreach ($files as $key) {
			$shared = $this->db->query("SELECT * FROM v_files WHERE id = '$key'")->first();
			if($shared->type=='folder') {
				if($shared->shared!='No') {
					$shared_users = explode('||', $shared->shared);
					$users_to_intro = explode('||', $integrantesproj);
					foreach ($users_to_intro as $uss) {
						if(!in_array($uss, $shared_users)) {
							$shared_users[]=$uss;
						}
					}
					$uss_to_int = implode('||', $shared_users);
					$this->db->update('v_files', $key, array(
						'status' => 'shared',
						'shared' => $uss_to_int
					));
				} else {
					$this->db->update('v_files', $key, array(
						'status' => 'shared',
						'shared' => $integrantesproj
					));
				}
				$urll = $shared->url . '/';
				$files = $this->db->query("SELECT * FROM v_files WHERE directory = '$urll'")->results();
				foreach ($files as $filee) {
					if($filee->filee!='No') {
						$shared_users = explode('||', $filee->shared);
						$users_to_intro = explode('||', $integrantesproj);
						foreach ($users_to_intro as $uss) {
							if(!in_array($uss, $shared_users)) {
								$shared_users[]=$uss;
							}
						}
						$uss_to_int = implode('||', $shared_users);
						$this->db->update('v_files', $filee->id, array(
							'status' => 'shared',
							'shared' => $uss_to_int
						));
					} else {
						$this->db->update('v_files', $filee->id, array(
							'status' => 'shared',
							'shared' => $integrantesproj
						));
					}
				}
			} else {
				if($shared->shared!='No') {
					$shared_users = explode('||', $shared->shared);
					$users_to_intro = explode('||', $integrantesproj);
					foreach ($users_to_intro as $uss) {
						if(!in_array($uss, $shared_users)) {
							$shared_users[]=$uss;
						}
					}
					$uss_to_int = implode('||', $shared_users);
					$this->db->update('v_files', $key, array(
						'status' => 'shared',
						'shared' => $uss_to_int
					));
				} else {
					$this->db->update('v_files', $key, array(
						'status' => 'shared',
						'shared' => $integrantesproj
					));
				}
			}
		}
	}



	public function delete_file($id, $url) {
		$this->db->delete('v_files', array('id', '=', $id));
		unlink($url);
	}

}








