<?php 
/**
 * Velocity Configuration Class - velocity-configuration-class.php
 * PHP Version 5 and +
 * @package v.config.class.php
 * @link https://velocity-framework.com/php/config.php
 * @author Juan Pablo Casabianca <jpc@pixelstudio32.com>
 * @copyright 2014 Juan Pablo Casabianca, GRUPO CASABIANCA IREGUI SAS
 * @copyright 2014 PHPACADEMY - As learned in there tutorial
 * @license http://www.http://creativecommons.org/licenses/by-nc-nd/4.0/
 * Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)
 */

namespace Velocity\Config;

class Config {

	/**
	 *
	 * Function that returns the values for the Global Configuration File
	 * Returns Config Value
	 *
	 * @access public
	 * @param  Config Value Path
	 * @return Config Value
	**/
	public static function get ($path = null) {

		if($path) {
			$config = $GLOBALS['config'];
			$path = explode('/', $path);

			foreach ($path as $bit) {
				if(isset($config[$bit])) {
					$config = $config[$bit];
				}
			}
			return $config;
		}
		return false;
	}

		/**
		 *
		 * Function that defines the Database
		 * Returns none
		 *
		 * @access public
		 * @param  Server Host, Databse Name, Database User, Database Password
		 * @return Connection Status
		**/
		public function define_database($host,$db_name,$db_user,$db_pass) {
			define("db_server",$host);
			define("db_name",$db_name);
			define("db_user",$db_user);
			define("db_pass",$db_pass); 
		}

		/**
		 *
		 * Function that defines a Constant
		 * Returns none
		 *
		 * @access public
		 * @param  Constant Name and Value
		 * @return None
		**/
		public function define_constant($name,$value) {
			define($value,$name);
		}

}