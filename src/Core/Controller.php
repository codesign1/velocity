<?php

namespace Velocity\Core;

use Velocity\Velocity;
use Velocity\Users\User;
use Velocity\Cms\Cms;

/**
 * Holds attributes common to all controllers in the app, the constructor function and
 * a render function that prints the template to the output using Twig
 */
class Controller {

	/**
	 * Array that stores all the <meta> information
	 * @var array
	 */
	public  $page_metas,
			$user,
			$user_data,
			$isLoggedIn,
			$cms;
 
	protected $page_name;

	protected $template_name;

	/**
	 * Receives the name of the controller, and the information to be retrieved later and put in
	 * the <meta> tags
	 * @param string $name       
	 * @param string $description
	 * @param string $keywords   
	 * @param string $author     
	 */
	public function __construct($name,  $description,  $keywords,  $author) {
		$page_metas = array();
		array_push($page_metas, $description, $keywords, $author);

		$this->user = new User();
		if($this->user->isLoggedIn()) {
			$this->isLoggedIn = true;
			$this->cms = new Cms($this->user->data()->id);
			$this->user_data = $this->user->data();
		} else {
			$this->isLoggedIn = false;
			$this->cms = new Cms();
		}

		$this->page_name = $name;
		$this->template_name = strtolower($name) . '.twig';
	}

	/**
	 * Twig knows where the templates are based on the initial configuration
	 * @return [type] [description]
	 */
	public function render($velocity_instance) {
		echo $velocity_instance->twig->render(
			$this->template_name,
			get_object_vars($this)
		);
	}
}
