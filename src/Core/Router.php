<?php

namespace Velocity\Core;

use Velocity\Velocity;
use Velocity\Authentication\Input;

class Router {

    private static $GET_URLS = array();
    private static $POST_URLS = array();
    private static $DELETE_URLS = array();
    private static $PUT_URLS = array();

    /**
     * For some weird reason, whenever the singleton is called again PHP instantiates it,
     * so we have to save the calling instance
     */
    private static $velocity_instance;

    public static function init($instance) {
        self::$velocity_instance = $instance;
    }

    public static function configure($config_callback) {
        $config_callback();
    }

    /**
     * Sets up a new GET route for the specified URL.
     * @param  [type] $url           route users attempt to see
     * @param  [type] $resource_name name of the template and t
     * @return [type]                [description]
     */
    public static function get($url, $resource_name) {
        self::$GET_URLS[$url] = $resource_name;
    }

    public static function post($url, $resource_name) {
        self::$POST_URLS[$url] = $resource_name;
    }

    public static function delete($url, $resource_name) {
        self::$DELETE_URLS[$url] = $resource_name;
    }

    public static function put($url, $resource_name) {
        self::$PUT_URLS[$url] = $resource_name;
    }

    public static function route($method, $url) {
        // check if the URL type is in the corresponding array
        $array_name = $method . '_URLS';
        $array =& self::$$array_name;
        $val = null;
        $params = null;

        // test each one of the URLs stored, to see if the controller
        // fits with the one in the request
        foreach ($array as $key => $value) {
            $key = '#^' . $key . '$#'; // prepare for regex matching
            if (preg_match($key, $url, $params) == 1) {
                $val = $value;
                break;
            }
        }

        if ($val) {
            // strip the numeric indexes from params
            foreach ($params as $k => $v) {
                if (is_int($k)) {
                    unset($params[$k]);
                }
            }

            // separate the resource name and the method
            $vals = explode('#', $val);

            // define the class name to instantiate
            $class_name = self::$velocity_instance->application_name . '\\app\\controllers\\' 
            . ucfirst($vals[0]) . 'Ctrl';

            if(class_exists($class_name)) {
                // instantiate, init and render
                $ctrl = new $class_name(ucfirst($vals[0]), '', '', '');
                $ctrl->init();

                // execute the method depending on the param
                $method_name = $vals[1];
                call_user_func_array(array($ctrl, $method_name), $params);

                // a special controller named AJAX controller has all the capabilities of a
                // normal one, including access to the cart, but does not need a template
                if(strtolower($vals[0]) !== 'ajax') {
                    $ctrl->render(self::$velocity_instance);
                } else {
                    if(file_exists(self::$velocity_instance->user_app_directory . '/app/templates/ajax.twig')) {
                        $ctrl->render(self::$velocity_instance);
                    }
                }
            } else {
                header($_SERVER["SERVER_PROTOCOL"] . " 500 Server error", true, 500);
                echo 'The resource defined for the selected route was not found, are you sure there\'s a controller
                with that name?';
                exit();
            }
        } else {
            // echo a 404 response
            header($_SERVER["SERVER_PROTOCOL"] . " 404 Not Found", true, 404);
            $user_error_page = self::$velocity_instance->user_app_directory . '/404.php';
            if(file_exists($user_error_page)) {
                include $user_error_page;
            } else {
                echo '404 not found\nplease create a custom 404.php page in your app root folder';
            }
            exit();
        }
    }
}
