<?php
/**
 * Velocity Database Class - velocity-database-class.php
 * PHP Version 5 and +
 * @package v.db.class.php
 * @link https://velocity-framework.com/php/db.php
 * @author Juan Pablo Casabianca <jpc@pixelstudio32.com>
 * @copyright 2014 Juan Pablo Casabianca, GRUPO CASABIANCA IREGUI SAS
 * @copyright 2014 PHPACADEMY - As learned in there tutorial
 * @license http://www.http://creativecommons.org/licenses/by-nc-nd/4.0/
 * Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)
 */

namespace Velocity\Db;

use \PDO;
use Velocity\Config\Config;

class Db {

	public $isConnected;

	private static $_instance = null;
	private $_pdo,
			$_query, 
			$_error = false, 
			$_results,
			$_count = 0,
			$_lastInsertId;


	private function __construct() {
		$this->isConnected = true;
		try {
			$this->_pdo = new PDO('mysql:host='. Config::get('mysql/host') . ';dbname=' . Config::get('mysql/db'), Config::get('mysql/username'), Config::get('mysql/password'));
				
		} catch(PDOException $e) {
			$this->isConnected = false;
			die($e->getMessage());
		}
	}

	public static function getInstance() {
		if(!isset(self::$_instance)) {
			self::$_instance = new DB();
		}
		return self::$_instance;
	}

	public function query($sql, $params = array()) {
		$this->_error = false;
		if($this->_query = $this->_pdo->prepare($sql)) {
			$x = 1;
			if(count($params)) {
				foreach ($params as $param) {
					$this->_query->bindValue($x,$param);
					$x++;
				}
			}

			if($this->_query->execute()) {
				$this->_results = $this->_query->fetchALL(PDO::FETCH_OBJ);
				$this->_count = $this->_query->rowCount();
				$this->_lastInsertId = $this->lastInsertId('id');
			} else {
				$this->_error = true;
			}
			$this->_lastInsertId = $this->lastInsertId();
		}
		return $this;
	}

	public function action($action, $table, $where = array()) {
		if(count($where) === 3) {
			$operators = array('=', '<', '>', '>=', '<=');

			$field = $where[0];
			$operator = $where[1];
			$value = $where[2];

			if(in_array($operator, $operators)) {
				$sql = "{$action} FROM {$table} WHERE {$field} {$operator} ?";
				if(!$this->query($sql, array($value))->error()) {
					return $this;
				}	
			}
		}
		return false;
	}

	public function get($table, $where) {
		return $this->action('SELECT *', $table, $where);
	}

	

	//$getrow = $database->getRow("SELECT email, username FROM users WHERE username =?", array("yusaf"));
	public function getRow($query, $params=array()){
        try { 
            $stmt = $this->_pdo->prepare($query); 
            $stmt->execute($params);
            return $stmt->fetch();  
        } catch(PDOException $e){
            throw new Exception($e->getMessage());
        }
    }

    // $getrows = $database->getRows("SELECT id, username FROM users");
    public function getRows($query, $params=array()){
        try { 
            $stmt = $this->_pdo->prepare($query); 
            $stmt->execute($params);
            return $stmt->fetchAll();       
        } catch(PDOException $e){
            throw new Exception($e->getMessage());
        }       
    }

	public function delete($table, $where) {
		return $this->action('DELETE', $table, $where);
	}

	public function insert($table, $fields = array()) {
		if(count($fields)) {
			$keys = array_keys($fields);
			$values = null;
			$x = 1;

			foreach($fields as $field) {
				$values .= "?";
				if($x < count($fields)) {
					$values .= ', ';
				}
				$x++;
			}



			$sql = "INSERT INTO {$table} (`" . implode('`, `', $keys) . "`) VALUES ({$values})";
			if(!$this->query($sql, $fields)->error()) {
				return true;
			}
		}
		return false;
	}

	public function update($table, $id, $fields, $key = 'id') {
		$set = '';
		$x = 1;

		foreach ($fields as $name => $value) {
			$set .= "{$name} = ?";
			if($x < count($fields)) {
				$set .= ', ';
			}	
			$x++;
		}

		$sql = "UPDATE {$table} SET {$set} WHERE {$key} = {$id}";
		if(!$this->query($sql, $fields)->error()) {
			return true;
		}
		return false;
	}

	public function find_social($id_name, $id) {
		$data = $this->get('v_users', array($id_name, '=', $id));
		if($data->count()) {
			return true;
		}
		return false;
	}

	//$insertrow = $database ->insertRow("INSERT INTO users (username, email) VALUES (?, ?)", array("yusaf", "yusaf@email.com"));
	public function insertRow($query, $params){
        try{ 
            $stmt = $this->_pdo->prepare($query); 
            $stmt->execute($params);
            }catch(PDOException $e){
            throw new Exception($e->getMessage());
        }           
    }

    public function get_last_id($table) {
    	$ids = $this->getRows("SELECT * FROM {$table} ORDER BY id DESC LIMIT 1");
    	return $ids[0]['id'];
    }

    public function Disconnect(){
            $this->_pdo = null;
            $this->isConnected = false;
        }

	public function results() {
		return $this->_results;
	}

	public function lastInsertId() {
		return $this->_lastInsertId;
	}

	public function first() {
		return $this->results()[0];
	}

	public function error() {
		return $this->_error;
	}

	public function count() {
		return $this->_count;
	}

	public function db_backup($tableName,$backupFile) {  
		$query = "LOAD DATA INFILE 'backupFile' INTO TABLE $tableName"; 
		return $db->query($query); 
	}
}










