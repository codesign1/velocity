<?php
/**
 * Velocity Sessions Class - velocity-session-class.php
 * PHP Version 5 and +
 * @package v.session.class.php
 * @link https://velocity-framework.com/php/session.php
 * @author Juan Pablo Casabianca <jpc@pixelstudio32.com>
 * @copyright 2014 Juan Pablo Casabianca, GRUPO CASABIANCA IREGUI SAS
 * @copyright 2014 PHPACADEMY - As learned in there tutorial
 * @license http://www.http://creativecommons.org/licenses/by-nc-nd/4.0/
 * Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)
 */

namespace Velocity\Cms;

use Velocity\Db\Db;

class Cms {

	private $db,
			$user_id;

	public function __construct($user = '') {
		$this->db = Db::getInstance();
		if($user!='') {
			$this->user_id = $user;
		}
	}

	public function create($table, $fields) {
		return $this->db->insert($table, $fields);
	}

	public function update($table, $id, $fields, $key = 'id') {
		return $this->db->update($table, $id, $fields, $key);
	}

	public function delete($table, $fields) {
		return $this->db->delete($table, $fields);
	}

	public function query($query) {
		return $this->db->query($query)->results();
	}

	public function get($table, $fields) {
		return $this->db->get($table, $fields)->results;
	}

	public function get_paginas() {
		return $this->db->query("SELECT * FROM v_pages")->results();
	}

	public function get_albums() {
		return $this->db->query("SELECT * FROM v_album")->results();
	}

	public function get_templates() {
		$files = scandir("../pages/");
		$array = array();
		foreach ($files as $node) {
			if ($node == '.' || $node == '.htaccess' || $node == '..' || $node == '.tmb' || $node == '.quarantine') continue;
			$template_name = explode('.', $node);
			$array[] = $template_name[0];
		}
		return $array;
	}

	public function get_fotos_album($id) {
		return $this->db->get("v_fotos", array('album', '=', $id))->results();
	}

	public function get_album($id) {
		return $this->db->get("v_album", array('id', '=', $id))->first();
	}

	public function get_last_album_id() {
		return $this->db->query("SELECT * FROM v_album ORDER BY id DESC")->first()->id;
	}

	public function get_blog() {
		return $this->db->query("SELECT * FROM v_blog ORDER BY id DESC")->results();
	}

	public function get_files($directory) {
		$files = scandir($directory);
		$files_array = array();
		foreach ($files as $file) {
			if($file!='.'&&$file!='..') {
				$files_array[]=$file;
			}
		}
		return $files_array;
	}

	public function get_usuarios() {
		$users = $this->db->query("SELECT * FROM v_users_admin ORDER BY name ASC")->results();
		$us = array();
		foreach ($users as $user) {
			if($user->id != $this->user_id && $user->group != 1) {
				$us[] = $user;
			}
		}
		return $us;
	}

	public function get_user($user_id) {
		return $this->db->get("v_users_admin", array('id', '=', $user_id))->first();
	} 

	public function get_configuracion() {
		return $this->db->get("v_configuracion", array('id', '=', 1))->first();
	}

	public function get_blog_post($url, $identifier = 'url') {
		return $this->db->get("v_blog", array($identifier, '=', $url))->first();
	}

	public function get_datasets($filtergraph, $labels, $tiempo) {
		$datasets = array();
		$data = array();
		if($filtergraph=='ventas') {
			for ($i=0; $i < count($labels); $i++) { 
				$year = date('Y', strtotime($labels[$i]));
				$month = date('m', strtotime($labels[$i])); 
				$day = date('d', strtotime($labels[$i]));
				if($tiempo == 'mes' || $tiempo == 'semana') {
					$res = $this->db->query("SELECT precio_mas_envio FROM v_pedidos_master WHERE ano = '$year' AND mes = '$month' AND dia = '$day'")->results();
					$ventas = 0;
					foreach ($res as $k) {
						$ventas += $k->precio_mas_envio;
					}
					$data[] = $ventas;
				} else {
					$res = $this->db->query("SELECT precio_mas_envio FROM v_pedidos_master WHERE ano = '$year' AND mes = '$month'")->results();
					$ventas = 0;
					foreach ($res as $k) {
						$ventas += $k->precio_mas_envio;
					}
					$data[] = $ventas;
				}
			}
			$datasets[] = $data;
		} elseif ($filtergraph=='pedidos') {
			for ($i=0; $i < count($labels); $i++) { 
				$year = date('Y', strtotime($labels[$i]));
				$month = date('m', strtotime($labels[$i])); 
				$day = date('d', strtotime($labels[$i]));
				if($tiempo == 'mes' || $tiempo == 'semana') {
					$res = $this->db->query("SELECT precio_mas_envio FROM v_pedidos_master WHERE ano = '$year' AND mes = '$month' AND dia = '$day'")->results();
					$data[] = count($res);
				} else {
					$res = $this->db->query("SELECT precio_mas_envio FROM v_pedidos_master WHERE ano = '$year' AND mes = '$month'")->results();
					$data[] = count($res);
				}
			}
			$datasets[] = $data;
		} else {
			for ($i=0; $i < count($labels); $i++) { 
				$year = date('Y', strtotime($labels[$i]));
				$month = date('m', strtotime($labels[$i])); 
				$day = date('d', strtotime($labels[$i]));
				if($tiempo == 'mes' || $tiempo == 'semana') {
					$res = $this->db->query("SELECT precio_mas_envio FROM v_pedidos_master WHERE ano = '$year' AND mes = '$month' AND dia = '$day' AND suscripcion = 1")->results();
					$data[] = count($res);
				} else {
					$res = $this->db->query("SELECT precio_mas_envio FROM v_pedidos_master WHERE ano = '$year' AND mes = '$month' AND suscripcion = 1")->results();
					$data[] = count($res);
				}
			}
			$datasets[] = $data;
		}
		return $datasets;
	}

	public function get_pedido($unique_id) {
		return $this->db->query("SELECT * FROM v_pedidos WHERE unique_id = '$unique_id' ORDER BY id ASC")->results();
	}

	public function get_pedidos($filter) {
		if($filter=='todos') {
			return $this->db->query("SELECT * FROM v_pedidos_master ORDER BY id ASC")->results();
		} elseif ($filter=='pendientes') {
			return $this->db->query("SELECT * FROM v_pedidos_master WHERE entregado = 0 ORDER BY id ASC")->results();
		} elseif ($filter=='completos') {
			return $this->db->query("SELECT * FROM v_pedidos_master WHERE entregado = 1 ORDER BY id ASC")->results();
		} else {
			return $this->db->query("SELECT * FROM v_pedidos_master WHERE suscripcion = 1 ORDER BY id ASC")->results();
		}
	}

	public function get_producto($sku) {
		if(stripos($sku, 'combo')===false) {
			return $this->db->get("v_productos", array('sku', '=', $sku))->first();
		} else {
			return $this->db->get("v_productos_complejos", array('sku', '=', $sku))->first();
		}
	}

	public function get_productos($type) {
		if($type=="sencillos") {
			return $this->db->query("SELECT * FROM v_productos ORDER BY sku ASC")->results();
		} else {
			return $this->db->query("SELECT * FROM v_productos_complejos ORDER BY sku ASC")->results();
		}
	}

	public function get_pedido_master($unique_id) {
		return $this->db->get("v_pedidos_master", array('unique_id', '=', $unique_id))->first();
	}

	public function get_categories() {
		return $this->db->query("SELECT * FROM v_categories ORDER BY url ASC")->results();
	}	

	public function get_sub_categories($parent_url) {
		return $this->db->get('v_sub_categorias', array('parent_url', '=', $parent_url))->results();
	}

}









