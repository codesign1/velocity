<?php
/**
 * Velocity Redirects Class - velocity-redirects-class.php
 * PHP Version 5 and +
 * @package v.redirect.class.php
 * @link https://velocity-framework.com/php/redirect.php
 * @author Juan Pablo Casabianca <jpc@pixelstudio32.com>
 * @copyright 2014 Juan Pablo Casabianca, GRUPO CASABIANCA IREGUI SAS
 * @copyright 2014 Niczak github: https://gist.github.com/niczak/2501891
 * @license http://www.http://creativecommons.org/licenses/by-nc-nd/4.0/
 * Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)
*/

namespace Velocity\Helpers;

class Timedate {

	private static $_instance = null;

	public  $time_zone,
			$time_created,
			$mysql_time_format,
			$time_stored;

	private function __construct($time_zone = '') {
		if($time_zone=='') {
			$this->time_zone = date_default_timezone_get();
		} else {
			$this->time_zone = date_default_timezone_get($time_zone);
		}
		$this->time_created = time();
		$this->mysql_time_format = $this::get_mysql_format();
	}

	public static function getInstance() {
		if(!isset(self::$_instance)) {
			self::$_instance = new Timedate();
		}
		return self::$_instance;
	}

	public function store_time($time) {
		$this->store_time = $time;
	}	

	public function get_time_zone() {
		return $this->time_zone;
	}

	public static function get_dates_week($date) {
	    $ts = strtotime($date);
	    $start = (date('w', $ts) == 0) ? $ts : strtotime('last sunday', $ts);
	    return array(date('Y-m-j', $start), date('Y-m-j', strtotime('next saturday', $start)));
	}

	public static function get_day($mode = 'number') {
		if($mode == 'number') {
			return date('d');
		} else {
			return date('l');
		}
	}

	public static function get_month($mode = 'number') {
		if($mode == 'number') {
			return date('m');
		} else {
			return date('F');
		}
	}

	public static function set_timezone($timezone) {
		date_default_timezone_get($timezone);
	}

	public static function get_year() {
		return date('Y');
	}

	public static function get_second() {
		return date('s');
	}

	public static function get_minutes() {
		return date('i');
	}

	public static function get_hour() {
		return date('H');		
	}

	public static function get_mysql_format() {
		return date("Y-m-d H:i:s");
	}

	public static function get_visitor_format() {
		return date("Y-m-d");
	}

	public static function time_past($event) {
		return Timedate::format_time_past(strtotime($event));
	}
	
	public static function format_time_past ($time) {
	    $time = time() - $time; // to get the time since that moment
	    return Timedate::beautify_time($time);
	}

	public static function beautify_time($time) {
		$tokens = array (
	        31536000 => 'year',
	        2592000 => 'month',
	        604800 => 'week',
	        86400 => 'day',
	        3600 => 'hour',
	        60 => 'minute',
	        1 => 'second'
	    );
	    foreach ($tokens as $unit => $text) {
	        if ($time < $unit) continue;
	        if ($time == 0) {
	        	return 'Just Now';
	        }
	        $numberOfUnits = floor($time / $unit);
	        return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'');
	    }
	}

	public static function time_past_from_now($start) {
		$now = strtotime(Timedate::get_mysql_format());
		return $now - strtotime($start);
	}

	public static function time_past_from_now_nice($time) {
		$now = strtotime(Timedate::get_mysql_format());
		$time_past = $now - strtotime($time);
		return Timedate::beautify_time($time_past);
	}

	public static function remaining_time($time) {
		$now = strtotime(Timedate::get_mysql_format());
		$time_past = strtotime($time) - $now;
		return Timedate::beautify_time($time_past);
	}


	public static function last_monday() {
		return date("Y-m-d",time() - ((date("N",time())-1)*86400));
	}

	public static function SpanishDate($fecha) {
		$fecha = strtotime($fecha);
		$nombre_dia = date('w', $fecha);
		$dia = date('d', $fecha);
		$nombre_mes = date('n', $fecha) - 1;
		$ano = date('Y', $fecha);

	   	$dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
		$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
		return $dia." de ".$meses[$nombre_mes]. " del ".$ano ;
	}

	public static function color_status($time) {
		$time_past = strtotime(Timedate::get_mysql_format()) - strtotime($time);
		if($time_past >= 432000) {
			return 'circlered';
		} elseif ($time_past >= 259200) {
			return 'circleorange';
		} else {
			return 'circlegreen';
		}	
	}

}