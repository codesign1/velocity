<?php
/**
* Velocity Database Class - velocity-new-database-class.php
* PHP Version 5 and +
* @package v.newdatabase.class.php
* @link https://velocity-framework.com/php/db.new.class.php
* @author Juan Pablo Casabianca <jpc@pixelstudio32.com>
* @copyright 2014 Juan Pablo Casabianca, GRUPO CASABIANCA IREGUI SAS
* @license http://www.http://creativecommons.org/licenses/by-nc-nd/4.0/
* Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)
*/

namespace Velocity\Helpers;

use Velocity\Config\Config;

class Helpers {

	/**
	 * Function that add an element to a normal array
	 * Returns the new array
	**/
	public function array_add($array,$add) {
		array_push($array, $add);
		return $array;
	}

	/**
	 * Function that divides the array by chunks of N
	 * Returns the new array
	**/
	public function array_get_chunks($array,$chunks) {
		$array = array_chunk($array, $chunks);
		return $array;
	}

	/**
	 * Function that gets a column by id
	 * Returns the column array
	**/
	public function get_array_column_keys($array,$key) {
		$array = array_column($array, $key);
		return $array;
	}

	/**
	 * Function that gets a column by value
	 * Returns the column array values
	**/
	public function get_array_column_values($array) {
		$array = array_values($array);
		return $array;
	}

	/**
	 * Function that combines two arrays with key and value
	 * Returns the new combined array
	**/
	public function combine_arrays($array,$array2) {
		$array_combine = array_combine($array, $array2);
		return $array_combine;
	}

	/**
	 * Function that counts values in the array (Case Insensitive)
	 * Returns array of key=object and value=times it appears
	**/
	public function count_values_in_array($array) {
		$count_array = array_count_values(array_map('strtolower', $array));
		return $count_array;
	}

	/**
	 * Function that creates and array with keys from one value
	 * Returns array of key and values
	**/
	public function keys_array_from_value($keys,$value) {
		$array_with_keys = array_fill_keys($keys, $value);
		return $array_with_keys;
	}

	/**
	 * Function that filters and array by odd or even
	 * Returns array of filtered values
	**/
	public function filter_array($array,$order_by) {
		$new_array = array();
		if($order_by!='even' && $order_by!='odd') {
			$order_by = 'even';
		}
		if($order_by == 'odd') {
			$num = 0;
		} else {
			$num = 1;
		}
		for ($i=0; $i < count($array); $i++) { 
			if($i % 2 == $num) {
				$new_array[] = $array[$i];
			} 
		}

		return $new_array;
	}

	/**
	 * Function that checks if key exists in array
	 * Returns true or false
	**/
	public function check_key_in_array($array,$key) {
		return array_key_exists($key, $array);
	}

	/**
	 * Function that gets keys of the array
	 * Returns array of keys
	**/
	public function get_array_keys($array) {
		return array_keys($array);
	}

	/**
	 * Function that removes last element of array
	 * Returns new array
	**/
	public function remove_last_element_array($array) {
		array_pop($array);
		return $array;
	}

	/**
	 * Function that gets random elements of an array
	 * Returns new array
	**/
	public function get_random_elements_array($array,$num_of_elements) {
		$rand_keys = array_rand($array, $num_of_elements);
		return $rand_keys;
	}

	/**
	 * Function that replaces or add if they dont exist elements to array
	 * Returns new array
	**/
	public function replace_elements_array($array,$array2) {
		$new_array = array_replace($array, $array2);
		return $new_array;
	}

	/**
	 * Function that reverse array order
	 * Returns new array
	**/
	public function reverse_array_order($array) {
		$new_array = array_reverse($array);
		return $new_array;
	}

	/**
	 * Function that checks if array is associative
	 * Returns true if associative
	**/
	function array_type( $obj ){
	    $last_key = -1;
	    $type = 'index';
	    foreach( $obj as $key => $val ){
	        if( !is_int( $key ) ){
	            return 'assoc';
	        }
	        if( $key !== $last_key + 1 ){
	            $type = 'sparse';
	        }
	        $last_key = $key;
	    }
	    return $type;
	}

	/**
	 * Function that cehcks array type
	 * Returns array type
	**/
	public function array_type_advanced($array) {
		foreach ($array as $key) {
			if(is_array($key)) {
				$type = 'multi';
			} else {
				if($this->array_type($array)=='assoc') {
					$type = 'assoc';
				} else {
					$type = 'index';
				}
			}
		}
		return $type;
	}

	/**
	 * Function that cehcks array type
	 * Returns array type
	**/
	public function search_array($array,$search) {
		$type = $this->array_type_advanced($array);
		$found=array();
		if($type=='index') {
			for ($i=0; $i < count($array) ; $i++) {
				if(stripos($array[$i], $search)!== false) {
					$found[]=$array[$i];
				}
			}
		} elseif ($type=='assoc') {
			foreach ($array as $key => $value) {
				$count=0;
			 	if (stripos($key, $search)!== false) {
			 		$found[]=$array[$key];
			 		$count++;
			 	} elseif(stripos($value, $search)!== false && $count==0) {
			 		$found[]=$array[$key];
			 	}
			}
		} elseif ($type=='multi') {
			foreach ($array as $key) {
				if(array_search($search, $key)!=false)
				$found[]=$key;
			}
		}
		return $found;
	}

	/**
	 * Function that returns a portion of an array
	 * Returns new array
	**/
	public function array_portion($array,$start,$end) {
		$new_array = array();
		for ($i=$start; $i <= $end; $i++) { 
			$new_array[]=$array[$i];
		}
		return $new_array;
	}

	/**
	 * Function that sorts and array by values in ASC order
	 * Returns new array
	**/
	public function sort_array_values_ASC($array) {
		asort($array);
		return $array;
	}

	/**
	 * Function that sorts and array by values in DESC order
	 * Returns new array
	**/
	public function sort_array_values_DESC($array) {
		arsort($array);
		return $array;
	}

	/**
	 * Function that sorts and array by values in DESC order
	 * Returns new array
	**/
	public function count_array($array) {
		return count($array);
	}

	/**
	 * Function that sorts and array by values in ASC order
	 * Returns new array
	**/
	public function sort_array_keys_ASC($array) {
		ksort($array);
		return $array;
	}

	/**
	 * Function that sorts and array by values in DESC order
	 * Returns new array
	**/
	public function sort_array_keys_DESC($array) {
		krsort($array);
		return $array;
	}

	/**
	 * Function that shuffles array order
	 * Returns new array
	**/
	public function shuffle_array($array) {
		shuffle($array);
		return $array;
	}

	public static function search_associative_array($array, $label, $find) {
		foreach ($array as $key) {
			if($key[$label] == $find) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Function that sorts and array by values in DESC order
	 * Returns new array
	**/
	public function check_type($value) {
		if(is_bool($value)) {
			return "boolean";
		} elseif (is_float($value)) {
			return "double";
		} elseif (is_int($value)) {
			return "int";
		} elseif (is_null($value)) {
			return "null";
		} elseif (is_long($value)) {
			return "long";
		} elseif (is_numeric($value)) {
			return "numeric";
		} elseif (is_object($value)) {
			return "object";
		} elseif (is_string($value)) {
			return "string";
		} else {
			return "No Type Found";
		}
	}

	/**
	 * Function that return integer value of something
	 * Returns int value
	**/
	public function get_int_value($value) {
		return intval($value);
	}

	/**
	 * Function that return string value of something
	 * Returns string value
	**/
	public function get_string_value($value) {
		return strval($value);
	}

	/**
	 * Function that return boolean value of something
	 * Returns boolean value
	**/
	public function get_boolean_value($value) {
		return boolval($value);
	}

	/**
	 * Function that sets a value type
	 * Returns value
	**/
	public function set_type_value($value,$type) {
		settype($value, $type);
		return $value;
	}

	/**
	 * Function that converts array to string
	 * Returns value
	**/
	public function convert_array_to_string($array) {
		return implode("", $array);
	}

	/**
	 * Function that converts array to string with separators
	 * Returns value
	**/
	public function convert_array_to_string_separators($array) {
		return implode("||", $array);
	}

	/**
	 * Function that convert strings to arrays
	 * Returns value
	**/
	public function convert_string_to_array($string,$separators) {
		return explode($separators, $string);
	}

	/**
	 * Function that show only a part of a string
	 * Returns value
	**/
	public function show_part_of_string($string,$start,$end) {
		return substr($string,$start,$end);
	}

	/**
	 * Function that trims whitespace from string
	 * Returns value
	**/
	public function trim_whitespace_of_string($text) {
		return trim($text);
	}

	/**
	 * Function that shuffles chars in string
	 * Returns value
	**/
	public function shuffle_string($text) {
		return str_shuffle($text);
	}

	/**
	 * Function that generates a random string by length
	 * Returns Random String
	**/
	public static function generateRandomString($length = 10) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, strlen($characters) - 1)];
	    }
	    return $randomString;
	}

	function readable_random_string($length = 6){  
	    $conso=array("b","c","d","f","g","h","j","k","l",  
	    "m","n","p","r","s","t","v","w","x","y","z");  
	    $vocal=array("a","e","i","o","u");  
	    $password="";  
	    srand ((double)microtime()*1000000);  
	    $max = $length/2;  
	    for($i=1; $i<=$max; $i++)  
	    {  
	    $password.=$conso[rand(0,19)];  
	    $password.=$vocal[rand(0,4)];  
	    }  
	    return $password;  
	}



	/**
	 * Function that count words in a text
	 * Returns value
	**/
	public function count_words($text) {
		return str_word_count($text);
	}

	/**
	 * Function that outputs css files in the header
	 * Returns value
	**/
	public function output_css($path,$array) {
		foreach ($array as $key) {
			$text = $text . '<link rel="stylesheet" type="text/css" href="'.$path.$key.'">';
		}
		return $text;
	}

	/**
	 * Function that outputs css files in the header
	 * Returns value
	**/
	public function output_js($path,$array) {
		foreach ($array as $key) {
			$text = $text . '<script src="'.$path.$key.'"></script>';
		}
		return $text;
	}

	/**
	 * Function that returns meta tags for SEO purposes
	 * Returns the meta tags
	**/
	public function output_metas($description,$keywords,$author) {
		$meta = $meta . '<meta name="description" content="'.$description.'" />';
        $meta = $meta . '<meta name="keywords" content="'.$keywords.'" />';
        $meta = $meta . '<meta name="author" content="'.$author.'" />';
        return $meta;
	}

	/**
	 * Function that outputs favicon
	 * Returns favicon html text
	**/
	public function output_favicon($path) {
			return '<link rel="shortcut icon" href="'.$path.'favicon.ico" type="image/x-icon">
    				<link rel="icon" href="'.$path.'favicon.ico" type="image/x-icon">';
	}

	/**
	 * Function that outputs css files in the header
	 * Returns value
	**/
	public function output_header($title,$meta_description,$meta_keywords,$meta_author,$css,$css_path,$path_favicon,$theme) {
		$text = '<!DOCTYPE html>
				<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
				<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
				<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
				<!--[if gt IE 8]><!--> <html class="no-js" lang="es"> <!--<![endif]-->
				    <head>
				        <meta charset="utf-8">
				        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
				        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, target-densitydpi=medium-dpi, user-scalable=0" />';
		$text = $text . $this->output_metas($meta_description,$meta_keywords,$meta_keywords);
		$text = $text . $this->output_css($css_path,$css);
		$text = $text . '<script src="js/vendor/modernizr-2.6.2.min.js"></script>
				        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
				        <script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>';
		$text = $text . $this->output_favicon($path_favicon);
		$text = $text . '</head>
							<body class="text-regular '.$theme.'">';

		return $text;
	}

	/**
	 * Function that outputs favicon
	 * Returns favicon html text
	**/
	public function output_footer($js_path,$js) {
			$text = $text . $this->output_js($js_path,$js);
			$text = $text . '</body>
						</html>';
			return $text;
	}

	/**
	 * Function that outputs server Indices and Information
	 * Returns array with information
	**/
	public function get_server_indices() {
		$indicesServer = array(
			'Current Page Path' => $_SERVER['PHP_SELF'], 
			'argv' => $_SERVER['argv'], 
			'argc' => $_SERVER['argc'], 
			'Gateway Interface' => $_SERVER['GATEWAY_INTERFACE'], 
			'Server Adress' => $_SERVER['SERVER_ADDR'], 
			'Server Name' => $_SERVER['SERVER_NAME'], 
			'Server Software' => $_SERVER['SERVER_SOFTWARE'], 
			'Server Protocol' => $_SERVER['SERVER_PROTOCOL'], 
			'Request Method' => $_SERVER['REQUEST_METHOD'], 
			'Request Time' => $_SERVER['REQUEST_TIME'], 
			'Request Time Float' => $_SERVER['REQUEST_TIME_FLOAT'], 
			'Query String' => $_SERVER['QUERY_STRING'], 
			'Document Root' => $_SERVER['DOCUMENT_ROOT'], 
			'Http Accept' => $_SERVER['HTTP_ACCEPT'], 
			'Http Accept Charset' => $_SERVER['HTTP_ACCEPT_CHARSET'], 
			'Http Accept Encoding' => $_SERVER['HTTP_ACCEPT_ENCODING'], 
			'Http Accept Language' => $_SERVER['HTTP_ACCEPT_LANGUAGE'], 
			'Http Connection' => $_SERVER['HTTP_CONNECTION'], 
			'Http Host' => $_SERVER['HTTP_HOST'], 
			'Http Referer' => $_SERVER['HTTP_REFERER'], 
			'Http User Agent' => $_SERVER['HTTP_USER_AGENT'], 
			'Https' => $_SERVER['HTTPS'], 
			'Remote Address' => $_SERVER['REMOTE_ADDR'], 
			'Remote Host' => $_SERVER['REMOTE_HOST'], 
			'Remote Port' => $_SERVER['REMOTE_PORT'], 
			'Remote User' => $_SERVER['REMOTE_USER'], 
			'Redirect Remote User' => $_SERVER['REDIRECT_REMOTE_USER'], 
			'Script Filename' => $_SERVER['SCRIPT_FILENAME'], 
			'Server Admin' => $_SERVER['SERVER_ADMIN'], 
			'Server Port' => $_SERVER['SERVER_PORT'], 
			'Server Signature' => $_SERVER['SERVER_SIGNATURE'], 
			'Path Translated' => $_SERVER['PATH_TRANSLATED'], 
			'Script Name' => $_SERVER['SCRIPT_NAME'], 
			'Request Uri' => $_SERVER['REQUEST_URI'], 
			'Php Auth Digest' => $_SERVER['PHP_AUTH_DIGEST'], 
			'Php Auth User' => $_SERVER['PHP_AUTH_USER'], 
			'Php Auth Pw' => $_SERVER['PHP_AUTH_PW'], 
			'Auth Type' => $_SERVER['AUTH_TYPE'], 
			'Path Info' => $_SERVER['PATH_INFO'], 
			'Original Path Info' => $_SERVER['ORIG_PATH_INFO']) ;

		return $indicesServer;
	}

	/**
	 * Function that outputs site Url
	 * Returns site url
	**/
	public static function site_url() {
			return $_SERVER['REQUEST_URI'];
	}

	/**
	 * Function that outputs base Url
	 * Returns base url for site
	**/
	public static function base_url() {
			return $_SERVER['DOCUMENT_ROOT'];
	}

	public static function get_email_link($email) {
		$components = explode("@", $email);
		return 'http://www.' . $components[1];
	}


	public static function escape($string) {
		return htmlentities($string, ENT_QUOTES, 'UTF-8');
	}

	function get_page_title($url){
		if( !($data = file_get_contents($url)) ) return false;
		if( preg_match("#(.+)<\/title>#iU", $data, $t))  {
			return trim($t[1]);
		} else {
			return false;
		}
	}

	public function cleanInput($input) {

		$search = array(
		    '@<script[^>]*?>.*?</script>@si',   // Strip out javascript
		    '@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
		    '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
		    '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments
		);

	    $output = preg_replace($search, '', $input);
	    return $output;
	  }

	public function sanitize($input) {
	    if (is_array($input)) {
	        foreach($input as $var=>$val) {
	            $output[$var] = sanitize($val);
	        }
	    }
	    else {
	        if (get_magic_quotes_gpc()) {
	            $input = stripslashes($input);
	        }
	        $input  = $this->cleanInput($input);
	        $output = mysql_real_escape_string($input);
	    }
	    return $output;
	}

	public function getDistanceBetweenPointsNew($latitude1, $longitude1, $latitude2, $longitude2) {
	    $theta = $longitude1 - $longitude2;
	    $miles = (sin(deg2rad($latitude1)) * sin(deg2rad($latitude2))) + (cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * cos(deg2rad($theta)));
	    $miles = acos($miles);
	    $miles = rad2deg($miles);
	    $miles = $miles * 60 * 1.1515;
	    $feet = $miles * 5280;
	    $yards = $feet / 3;
	    $kilometers = $miles * 1.609344;
	    $meters = $kilometers * 1000;
	    return compact('miles','feet','yards','kilometers','meters'); 
	}

	// puts th st.... on numbers
	public function ordinal($cdnl){ 
	    $test_c = abs($cdnl) % 10; 
	    $ext = ((abs($cdnl) %100 < 21 && abs($cdnl) %100 > 4) ? 'th' 
	            : (($test_c < 4) ? ($test_c < 3) ? ($test_c < 2) ? ($test_c < 1) 
	            ? 'th' : 'st' : 'nd' : 'rd' : 'th')); 
	    return $cdnl.$ext; 
	}  

	public function get_page_links() {
		$html = file_get_contents('http://www.example.com');
		$dom = new DOMDocument();
		@$dom->loadHTML($html);

		// grab all the on the page
		$xpath = new DOMXPath($dom);
		$hrefs = $xpath->evaluate("/html/body//a");

		for ($i = 0; $i < $hrefs->length; $i++) {
		       $href = $hrefs->item($i);
		       $url = $href->getAttribute('href');
		       $urls[]=$url;
		}

		return $urls;
	}

	private function _make_url_clickable_cb($matches) {
		$ret = '';
		$url = $matches[2];
	 
		if ( empty($url) )
			return $matches[0];
		// removed trailing [.,;:] from URL
		if ( in_array(substr($url, -1), array('.', ',', ';', ':')) === true ) {
			$ret = substr($url, -1);
			$url = substr($url, 0, strlen($url)-1);
		}
		return $matches[1] . "<a href=\"$url\" rel=\"nofollow\">$url</a>" . $ret;
	}
	 
	private function _make_web_ftp_clickable_cb($matches) {
		$ret = '';
		$dest = $matches[2];
		$dest = 'http://' . $dest;
	 
		if ( empty($dest) )
			return $matches[0];
		// removed trailing [,;:] from URL
		if ( in_array(substr($dest, -1), array('.', ',', ';', ':')) === true ) {
			$ret = substr($dest, -1);
			$dest = substr($dest, 0, strlen($dest)-1);
		}
		return $matches[1] . "<a href=\"$dest\" rel=\"nofollow\">$dest</a>" . $ret;
	}
	 
	private function _make_email_clickable_cb($matches) {
		$email = $matches[2] . '@' . $matches[3];
		return $matches[1] . "<a href=\"mailto:$email\">$email</a>";
	}
	 
	public function make_clickable($ret) {
		$ret = ' ' . $ret;
		// in testing, using arrays here was found to be faster
		$ret = preg_replace_callback('#([\s>])([\w]+?://[\w\\x80-\\xff\#$%&~/.\-;:=,?@\[\]+]*)#is', '_make_url_clickable_cb', $ret);
		$ret = preg_replace_callback('#([\s>])((www|ftp)\.[\w\\x80-\\xff\#$%&~/.\-;:=,?@\[\]+]*)#is', '_make_web_ftp_clickable_cb', $ret);
		$ret = preg_replace_callback('#([\s>])([.0-9a-z_+-]+)@(([0-9a-z-]+\.)+[0-9a-z]{2,})#i', '_make_email_clickable_cb', $ret);
	 
		// this one is not in an array because we need it to run last, for cleanup of accidental links within links
		$ret = preg_replace("#(<a( [^>]+?>|>))<a [^>]+?>([^>]+?)</a></a>#i", "$1$3</a>", $ret);
		$ret = trim($ret);
		return $ret;
	}

	public function data_uri($file, $mime) {
		  $contents=file_get_contents($file);
		  $base64=base64_encode($contents);
		  echo "data:$mime;base64,$base64";
	}

	public function generateCsv($data, $delimiter = ',', $enclosure = '"') {
       $handle = fopen('php://temp', 'r+');
       foreach ($data as $line) {
               fputcsv($handle, $line, $delimiter, $enclosure);
       }
       rewind($handle);
       while (!feof($handle)) {
               $contents .= fread($handle, 8192);
       }
       fclose($handle);
       return $contents;
	}

	public function echo_args() {
	    // returns an array of all passed arguments
	    $args = func_get_args();
	    foreach ($args as $k => $v) {
	        echo "$v\n";
	    }
	}

	public function gzcompress_str($string) {
		return gzcompress($string);
	}

	public static function pr($array){
		echo '<pre>'.print_r($array,true).'</pre>';
	}

	public static function request_uri() {
		return 'http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
	}

	public static function display_source_code($url) {
		$lines = file($url); // pick teh url
		foreach ($lines as $line_num => $line) {
			// loop thru each line and prepend line numbers
			echo "Line #<b>{$line_num}</b> : " . htmlspecialchars($line) . "<br />\n";
		}
	}

	public function whois_query($domain) {
	 
	    // fix the domain name:
	    $domain = strtolower(trim($domain));
	    $domain = preg_replace('/^http:\/\//i', '', $domain);
	    $domain = preg_replace('/^www\./i', '', $domain);
	    $domain = explode('/', $domain);
	    $domain = trim($domain[0]);
	 
	    // split the TLD from domain name
	    $_domain = explode('.', $domain);
	    $lst = count($_domain)-1;
	    $ext = $_domain[$lst];
	 
	    // You find resources and lists 
	    // like these on wikipedia: 
	    //
	    // <a href="http://de.wikipedia.org/wiki/Whois">http://de.wikipedia.org/wiki/Whois</a>
	    //
	    $servers = array(
	        "biz" => "whois.neulevel.biz",
	        "com" => "whois.internic.net",
	        "us" => "whois.nic.us",
	        "coop" => "whois.nic.coop",
	        "info" => "whois.nic.info",
	        "name" => "whois.nic.name",
	        "net" => "whois.internic.net",
	        "gov" => "whois.nic.gov",
	        "edu" => "whois.internic.net",
	        "mil" => "rs.internic.net",
	        "int" => "whois.iana.org",
	        "ac" => "whois.nic.ac",
	        "ae" => "whois.uaenic.ae",
	        "at" => "whois.ripe.net",
	        "au" => "whois.aunic.net",
	        "be" => "whois.dns.be",
	        "bg" => "whois.ripe.net",
	        "br" => "whois.registro.br",
	        "bz" => "whois.belizenic.bz",
	        "ca" => "whois.cira.ca",
	        "cc" => "whois.nic.cc",
	        "ch" => "whois.nic.ch",
	        "cl" => "whois.nic.cl",
	        "cn" => "whois.cnnic.net.cn",
	        "cz" => "whois.nic.cz",
	        "de" => "whois.nic.de",
	        "fr" => "whois.nic.fr",
	        "hu" => "whois.nic.hu",
	        "ie" => "whois.domainregistry.ie",
	        "il" => "whois.isoc.org.il",
	        "in" => "whois.ncst.ernet.in",
	        "ir" => "whois.nic.ir",
	        "mc" => "whois.ripe.net",
	        "to" => "whois.tonic.to",
	        "tv" => "whois.tv",
	        "ru" => "whois.ripn.net",
	        "org" => "whois.pir.org",
	        "aero" => "whois.information.aero",
	        "nl" => "whois.domain-registry.nl"
	    );
	 
	    if (!isset($servers[$ext])){
	        die('Error: No matching nic server found!');
	    }
	 
	    $nic_server = $servers[$ext];
	 
	    $output = '';
	 
	    // connect to whois server:
	    if ($conn = fsockopen ($nic_server, 43)) {
	        fputs($conn, $domain."\r\n");
	        while(!feof($conn)) {
	            $output .= fgets($conn,128);
	        }
	        fclose($conn);
	    }
	    else { die('Error: Could not connect to ' . $nic_server . '!'); }
	 
	    return $output;
	}

	public function array_2_object($data) {
	    return is_array($data) ? (object) array_map(__FUNCTION__,$data) : $data;
	}

	public static function hex_2_rgb($color){
	    $color = str_replace('#', '', $color);
	    if (strlen($color) != 6){ return array(0,0,0); }
	    $rgb = array();
	    for ($x=0;$x<3;$x++){
	        $rgb[$x] = hexdec(substr($color,(2*$x),2));
	    }
	    return $rgb;
	}

	public static function rgb_2_hex($rgb) {
		return sprintf("%06X", $rgb);
	}

	public static function ColorDarken($color, $dif=20){
	    $color = str_replace('#', '', $color);
	    if (strlen($color) != 6){ return '000000'; }
	    $rgb = '';
	 
	    for ($x=0;$x<3;$x++){
	        $c = hexdec(substr($color,(2*$x),2)) - $dif;
	        $c = ($c < 0) ? 0 : dechex($c);
	        $rgb .= (strlen($c) < 2) ? '0'.$c : $c;
	    }
	 
	    return '#'.$rgb;
	}

	public static function generate_color(){
	    mt_srand((double)microtime()*1000000);
	    $color_code = '';
	    while(strlen($color_code)<6){
	        $color_code .= sprintf("%02X", mt_rand(0, 255));
	    }
	    return $color_code;
	}	

	public static function create_url($string) {

		$unwanted_array = array(    'Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
		                            'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U',
		                            'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c',
		                            'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o',
		                            'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y' );

		$title = strtr($string, $unwanted_array);
		//Title to friendly URL conversion
		$newtitle=Helpers::string_limit_words($title, 6); // First 6 words
		$urltitle=preg_replace('/[^a-z0-9]/i',' ', $newtitle);
		$newurltitle=str_replace(" ","-",$newtitle);
		return strtolower($newurltitle); // Final URL
	}

	public static function string_limit_words($string, $word_limit) {
		$words = explode(' ', $string);
		return implode(' ', array_slice($words, 0, $word_limit));
	}

	public static function img($img) {
		return Config::get('website_info/website_url') . '/public/img/' . $img;
	}

	public static function website() {
		return Config::get('website_info/website_url') . '/';
	}

	public static function url($url = '') {
		return Config::get('website_info/website_url') . '/' . $url;
	}

	public static function background_image($img) {
		return 'style="background-image:url('.Config::get('website_info/website_url').'/static_files/img/'.$img.');"';
	}

	public static function format_size_units($bytes) {
        if ($bytes >= 1073741824) {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        } elseif ($bytes >= 1048576) {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        } elseif ($bytes >= 1024) {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        } elseif ($bytes > 1) {
            $bytes = $bytes . ' bytes';
        } elseif ($bytes == 1) {
            $bytes = $bytes . ' byte';
        } else {
            $bytes = '0 bytes';
        }
        return $bytes;
	}

}

