<?php
/**
 * Velocity Redirects Class - velocity-redirects-class.php
 * PHP Version 5 and +
 * @package v.redirect.class.php
 * @link https://velocity-framework.com/php/redirect.php
 * @author Juan Pablo Casabianca <jpc@pixelstudio32.com>
 * @copyright 2014 Juan Pablo Casabianca, GRUPO CASABIANCA IREGUI SAS
 * @copyright 2014 PHPACADEMY - As learned in there tutorial
 * @license http://www.http://creativecommons.org/licenses/by-nc-nd/4.0/
 * Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)
 */

namespace Velocity\Helpers;

class Redirect {

	public static function to($location = null) {
		if($location) {
			if(is_numeric($location)) {
				switch($location) {
					case 404:
						header('HTTP/1.0 404 Not Found');
						include '404.html';
						exit();
					break;
					case 404:
						header('error');
						exit();
					break;
				}
			}
			header('Location: ' . $location);
			exit();
		}
	}

}