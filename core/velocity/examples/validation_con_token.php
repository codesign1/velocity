<?php

if(Input::exists()) {
	if(Token::check(Input::get('token'))) {
		$validate = new Validate();
		$validation = $validate->check($_POST, array(
			'username' => array(
				'required' => true,
				'min' => 2,
				'max' => 20,
				'unique' => 'v_users'
			),
			'password' => array(
				'required' => true,
				'min' => 6
			),
			'password_again' => array(
				'required' => true,
				'matches' => 'password'
			),
			'email' => array(
				'required' => true,
				'email' => 'valid'
			),
			'name' => array(
				'required' => true,
				'min' => 2,
				'max' => 50
			)
		));

		if($validation->passed()) {
				
			// ALGO SI VALIDATION PASÓ

			// Session::flash('home', 'You have been registered and can now log in!');
			// Redirect::to('index.php');
	
		} else {
			
			$errors = $validation->errors();

		}
	}
}

if(isset($errors)) {
	foreach ($errors as $error) {
		echo '<div class="grid-12-xs alert red"><span>'.$error.'</span></div>';
	}
} 

?>

<input type="hidden" name="token" value="<?php echo Token::generate(); ?>">