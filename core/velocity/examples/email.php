<?php

if(Input::exists()) {
	if(Token::check(Input::get('token'))) {
		$validate = new Validate();
		$validation = $validate->check($_POST, array(
			'username' => array(
				'required' => true,
				'min' => 2,
				'max' => 20,
				'unique' => 'v_users'
			),
			'password' => array(
				'required' => true,
				'min' => 6
			),
			'password_again' => array(
				'required' => true,
				'matches' => 'password'
			),
			'email' => array(
				'required' => true,
				'email' => 'valid'
			),
			'name' => array(
				'required' => true,
				'min' => 2,
				'max' => 50
			)
		));

		if($validation->passed()) {
				
			$mail = new PHPMailer;
			$mail->setFrom('email', 'nombre');
			$mail->addReplyTo('email', 'nombre');
			$mail->addAddress('email', 'nombre');
			$mail->isHTML(true);
			$mail->Subject = 'Subject';
			$body = '';
			$mail->Body = $body;
			if (!$mail->send()) {
			    $status = $mail->ErrorInfo;
			} else {
			    $status = 'success';
			}

			// SESSION::flash('home', 'You have been registered and can now log in!');
			// REDIRECT::to('index.php');
	
		} else {
			
			$errors = $validation->errors();

		}
	}
}
?>
<input type="hidden" name="token" value="<?php echo Token::generate(); ?>">