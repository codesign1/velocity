<?php 

if(Input::exists()) {
	if(Token::check(Input::get('token'))) {
		$validate = new Validate();
		$validation = $validate->check($_POST, array(
			'username' => array(
				'required' => true,
				'min' => 2,
				'max' => 20,
				'unique' => 'v_users'
			),
			'password' => array(
				'required' => true,
				'min' => 6
			),
			'password_again' => array(
				'required' => true,
				'matches' => 'password'
			),
			'email' => array(
				'required' => true,
				'email' => 'valid'
			),
			'name' => array(
				'required' => true,
				'min' => 2,
				'max' => 50
			)
		));

		if($validation->passed()) {
			
			$user = new User();
			$salt = Hash::salt(32);

			try {

				$user->create(array(
					'username' => Input::get('username') ,
					'password' => Hash::make(Input::get('password'), $salt) ,
					'email' => Input::get('email') ,
					'salt' => $salt,
					'name' => Input::get('name') ,
					'joined' => date('Y-m-d H:i:s'),
					'group' => 1
				));

				Session::flash('home', 'You have been registered and can now log in!');
				Redirect::to('index.php');

			} catch(Exception $e) {
				die($e->getMessage());
			}

		} else {
			foreach ($validation->errors() as $error) {
				echo $error . '<br>';
			}
		}
	}
}


?>

<form action="" method="post">
	<h3>Without Email Activation</h3>
	<div class="field">
		<label for="username">Username</label> <br>
		<input type="text" name="username" id="username" value="<?php echo Helpers::escape(Input::get('username')); ?>" autocomplete="off">
	</div>
	<div class="field">
		<label for="password">Choose a password</div>
		<input type="password" name="password" id="password">
	</div>
	<div class="field">
		<label for="password_again">Enter password again</div>
		<input type="password" name="password_again" id="password_again">
	</div>
	<div class="field">
		<label for="email">Your Email</div>
		<input type="email" name="email" value="<?php echo Helpers::escape(Input::get('email')); ?>" id="email">
	</div>
	<div class="field">
		<label for="name">Your Name</div>
		<input type="text" name="name" value="<?php echo Helpers::escape(Input::get('name')); ?>" id="name">
	</div> <br>

	<input type="hidden" name="token" value="<?php echo Token::generate(); ?>">
	<input type="submit" value="Register">

</form>



