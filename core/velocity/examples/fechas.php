<?php

// MYSQL FORMAT
Timedate::get_mysql_format();

// Cuanto tiempo ha pasado desde que paso algo
Timedate::time_past_from_now_nice($tiempo);

// Caunto tiempo falta para que pase algo
Timedate::remaining_time($tiempo);

// Fecha en español en formato lindo
Timedate::SpanishDate($fecha);