<?php

$user = new USER();
if($user->isLoggedIn()) {
	$user_data = $user->data();
	$shop = new ECOMMERCE($user_data->id);
} else {
	$shop = new ECOMMERCE();
}

list($cart, $cart_total) = $shop->get_cart();