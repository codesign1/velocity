<?php 

if(Input::exists()) {
	if(Token::check(Input::get('token'))) {
		$send_email = false;
		$validate = new Validate();
		$validation = $validate->check($_POST, array(
			'email' => array(
				'required' => true,
				'email' => 'valid'
			)
		));

		if($validation->passed()) {
			try {
				$send_email = true;
			} catch(Exception $e) {
				die($e->getMessage());
			}

		} else {
			foreach ($validation->errors() as $error) {
				echo $error . '<br>';
			}
		}

		if($send_email) {
		$user = new User(Input::get('email'));
			if(!$user->exists()) {
				Redirect::to(404);
			} else {
				$data = $user->data();
				$new_password = $user->generate_new_password($data->email);

				$email = new PHPMailer;
				$email->isSMTP();
				$email->SMTPAuth = true;
				$email->Host = 'smtp.gmail.com';
				$email->Username = 'juan.pablo.casabianca@gmail.com';
				$email->Password = 'jpcasabianca1991';
				$email->SMTPSecure = 'ssl';
				$email->Port = 465;
				$email->From = 'casabiancajuanpablo@gmail.com';
				$email->FromName = 'Juan Pablo at Velocity';
				$email->addReplyTo('reply@gmail.com', 'Reply Address');
				$email->addAddress($data->email, $data->name);
				$email->isHTML(true);
				$email->Subject = 'Password Recovery';
				$email->Body = '<h1>We are sorry to here you forgot your password!</h1><p>This is your new password: <strong>' . $new_password . '</strong></p>';
				if($email->send()) {
					Session::flash('login', 'Your new password has been sent to your email!');
					Redirect::to('login.php');
				} 
			}
		}
	}
}
?>

<p>Enter your email so we can send you your new password!</p>
<form action="" method="post">
	<div class="field">
		<label for="email">Email</label>
		<input type="email" name="email" id="email">
	</div>
	<input type="hidden" name="token" value="<?php echo Token::generate(); ?>">
	<input type="submit" value="Send New Password">

</form>


<h1>Funciona!!</h1>